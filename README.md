# Drinkfull

![Drinkfull logo](https://i.imgur.com/pWKSafz.png)

(see installation below)

# Initial idea

The idea is to have a system where multiple people can tally off their consumption of beverages among different locations. Each user gets an invoice from each location in a timely fashion.

# Possible application

Among dorms, companies and clubs there are different fridges where each member can purchase a beverage and this works on trust basis at the moment. The issue is that there is no centered database where all this information (price, location, time etc.) is kept and varies from one fridge to the other one. Thus the idea to create an app where each user tallies off how many beverages they took from the specific location and also gets one invoice from all the locations.

# Features

## User

-   Login with different accounts (Email based authentication)
-   Purchase beverages for oneself while being in different locations (product/price) differences
-   Personal history of consumption order by activity (date/sum)
-   show current invoice and download invoice pdf
-   Standard account settings (password change, monthly limit, banking data, adress)

## Admin

-   Stock up fridges, add products or delete inventory
-   Overview of fridge revenue generated in a specific month (banking)
-   Add, edit and remove users
-   change account password

# Tech stack

-   Frontend: Angular 5 + Ionic 3
-   Backend: MongoDB + node.js express server + restful API
-   User authentication (read/insert/update) with json web tokens
-   cross platform compatibility/responsive design
-   jsPDF plugin for invoice building
-   ngx-cookie-service
-   karma+jasmin for testing
-   git for version control

# MongoDB schema

![Database table](https://i.imgur.com/iTMdpSi.png)

# Installation (macOS)

1.Unpack drinkfull.zip into a directory of your choice

2.Open the directory in terminal and run

3.Install homebrew

```
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

4.Install node

```
brew install node
```

5.Install Ionic

```
sudo npm install -g cordova ionic
```

6.Install missing packages node_modules

```
npm i
```

7.Install Google Chrome + **Allow-Control-Allow-Origin** extension to solve CORS issues when run locally + external mongoDB

<https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi>

8.Start ionic with the command

```
ionic serve
```

9.Open your server url in the browser and enjoy Drinkfull :-) Admin login is:

User:

```
admin@wohnheim.de
```

Password:

```
abc
```

# Deployment to server

1.Change the server base url/path and if necessary the user/password for mongo in `www/AuthController.js` and ionic project

2.Build project possibly with `ionic serve`

3.upload the _www_ folder in drinkfull + json files to a webserver of your choice.

4.Change to _www_ directory and run `npm i`

5.Install mongo on the webserver and import the 4 collections into the database `pwpg08`. Example for products.json

```
mongoimport --db pwpg08 --collection products --file products.json --jsonArray
```

6.Install `npm i pm2` and start the node server in the www folder:

```
pm2 start server.js  --name node-server
```

7.Stop server with the command:

```
pm2 stop all
```

# Testing

Run the following command in the drinkfull folder to start Karma+Jasmin testing environment:

```
npm test
```

# Remarks

-   In _./src/pages/history/segments/invoiceSegment.ts_ uncomment

```js
// let head = [['ID', 'Getränk', 'Haus', 'Anzahl', 'Preis in €']];
// doc.autoTable({ head: head, body: body, startY: 90 });
```

before trying out the download invoice functionality. Might need to rebuild once uncommented. The error persistes because of a tslint issue.

-   When testing add the jasmine-types

```
npm i @types/jasmine
```

to get tslint functionality.

This feature is purposely removed from the _package.json_ to prevent building issues!

# Developers

[Franjo Lukezic](https://gitlab.com/lynxdingo), [Alex Hoebel](https://gitlab.cip.ifi.lmu.de/hoebel), [Fan Fan](https://gitlab.cip.ifi.lmu.de/fanfa)

# Licensing

MIT License:
<https://en.wikipedia.org/wiki/MIT_License>
