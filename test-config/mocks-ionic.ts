import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ProductService } from '../src/services/productService';
import { Product } from '../src/aids/product';
import { UserService } from '../src/services/userService';
import { User } from '../src/aids/user';
import { OrderService } from '../src/services/orderService';
import { LocationService } from '../src/services/locationService';
import { AuthService } from '../src/services/authService';
import { Location } from '../src/aids/location';
import { Order } from '../src/aids/order';
import {
    App,
    AlertController,
    LoadingController,
    NavController,
    ViewController,
    ToastController,
    Config,
    NavParams,
    PopoverController
} from 'ionic-angular';
import { CookieService } from 'ngx-cookie-service';
import { HttpClient, HttpHandler } from '@angular/common/http';

export class PlatformMock {
    public ready(): Promise<string> {
        return new Promise(resolve => {
            resolve('READY');
        });
    }

    public getQueryParam() {
        return true;
    }

    public registerBackButtonAction(fn: Function, priority?: number): Function {
        return () => true;
    }

    public hasFocus(ele: HTMLElement): boolean {
        return true;
    }

    public doc(): HTMLDocument {
        return document;
    }

    public is(): boolean {
        return true;
    }

    public getElementComputedStyle(container: any): any {
        return {
            paddingLeft: '10',
            paddingTop: '10',
            paddingRight: '10',
            paddingBottom: '10'
        };
    }

    public onResize(callback: any) {
        return callback;
    }

    public registerListener(
        ele: any,
        eventName: string,
        callback: any
    ): Function {
        return () => true;
    }

    public win(): Window {
        return window;
    }

    public raf(callback: any): number {
        return 1;
    }

    public timeout(callback: any, timer: number): any {
        return setTimeout(callback, timer);
    }

    public cancelTimeout(id: any) {
        // do nothing
    }

    public getActiveElement(): any {
        return document['activeElement'];
    }
}

export class StatusBarMock extends StatusBar {
    styleDefault() {
        return;
    }
}

export class SplashScreenMock extends SplashScreen {
    hide() {
        return;
    }
}

export class NavMock {
    public pop(): any {
        return new Promise(function(resolve: Function): void {
            resolve();
        });
    }

    public push(): any {
        return new Promise(function(resolve: Function): void {
            resolve();
        });
    }

    public getActive(): any {
        return {
            instance: {
                model: 'something'
            }
        };
    }

    public setRoot(): any {
        return true;
    }

    public registerChildNav(nav: any): void {
        return;
    }
}

export class DeepLinkerMock {}

// --- SERVICES --- //
export class AuthServiceMock extends AuthService {
    auth(): Promise<boolean> {
        return Promise.resolve(true);
    }
}
export class LocationServiceMock extends LocationService {
    // TODO: create a locations array
    getLocations(): Promise<Location[]> {
        return Promise.resolve([]);
    }
}
export class OrderServiceMock extends OrderService {
    // TODO: create a orders array
    getOrders(): Promise<Order[]> {
        let tOrders: Order[] = [
            {
                _id: '1',
                productId: '2',
                locationId: '3',
                userId: '4',
                orderDate: '5',
                price: '6',
                deleted: '0'
            }
        ];

        return Promise.resolve(tOrders);
    }
    insertOrder(): Promise<boolean> {
        return Promise.resolve(true);
    }
    deleteOrder(): Promise<boolean> {
        return Promise.resolve(true);
    }
    createOrder(): Promise<boolean> {
        return Promise.resolve(true);
    }
}
export class ProductServiceMock extends ProductService {
    // TODO: create a products array
    getProducts(): Promise<Product[]> {
        return Promise.resolve([]);
    }
    insertProduct(): Promise<boolean> {
        return Promise.resolve(true);
    }
    updateProduct(): Promise<boolean> {
        return Promise.resolve(true);
    }
    getProductInformation(): Product {
        let tProduct: Product = {
            _id: '1',
            productName: '',
            locationId: '',
            price: '',
            count: 2,
            countMax: 3,
            dateAdded: 4,
            dateUpdated: 5,
            deleted: 'false'
        };

        return tProduct;
    }
}

export class UserServiceMock extends UserService {
    // TODO: create a users array
    getUsers(): Promise<User[]> {
        return Promise.resolve([]);
    }
    insertUser(): Promise<boolean> {
        return Promise.resolve(true);
    }
    updateUser(): Promise<boolean> {
        return Promise.resolve(true);
    }
    getUserFromToken(): Promise<User> {
        let tUser: User = {
            _id: '1',
            email: 'test@example.com',
            password: 'password'
        };
        return Promise.resolve(tUser);
    }
}

// --- Angular Mocks --- //
export class AppMock extends App {}
export class AlertControllerMock extends AlertController {}
export class LoadingControllerMock extends LoadingController {}
export abstract class NavControllerMock extends NavController {}
export class NavParamsMock extends NavParams {}
export class ToastControllerMock extends ToastController {}
export class CookieServiceMock extends CookieService {}
export class ConfigMock extends Config {}
export class HttpClientMock extends HttpClient {}
export abstract class HttpHandlerMock extends HttpHandler {}
export class PopoverControllerMock extends PopoverController {}
