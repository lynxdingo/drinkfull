var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
var User = require('./User');

//Import json web token requirements
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var VerifyToken = require('./VerifyToken');
var config = { 'secret': 'supersecret' };

//Import mongodb
var mongo = require('mongodb');
var MongoClient = require('mongodb').MongoClient;

//Login data
const user = encodeURIComponent('pwpg08');
const password = encodeURIComponent('43g0fra3n');
const authMechanism = 'DEFAULT';

// Connection URL to database 
//Including user/password authentication
const url = `mongodb://${user}:${password}@localhost:27017/pwpg08?authMechanism=${authMechanism}`;
//Without authentication, local support
//const url = `mongodb://localhost:27017/`;


//read database for debug mode without token or authentication (no token yet)
//return collection object or specific user object for authentication
function readDB(collection,query) {
    return new Promise(resolve => {
        MongoClient.connect(url, function(err, db) {
          if (err) throw err;
          //use database pwpg08
          var database = db.db('pwpg08');

          //return user data from _id with /me/ endpoint
          if(collection == "users" && query){

            let id = new mongo.ObjectID(query);
            var myquery = { "_id" : id };


            database.collection(collection).find(myquery).toArray(function(err, result) {
              if (err) throw err;
              db.close();
              delete result[0].password;
              resolve(result);
            });
          }
          //return complete collection
          else{
            database.collection(collection).find().toArray(function(err, result) {
              if (err) throw err;
              db.close();
              resolve(result);
            });
          }


        });
    });
};


//read database with token data as authentication
function readDBToken(collection,token) {

    //console.log("token:")
    //console.log(token)

    //verify if token is valid (expiration date, secret key..)
    return jwt.verify(token, config.secret, function(err, decoded) {

        //return reject if token is invalid
        if (err) {
            console.log("error verifying token data");
            return Promise.reject({ "success": "false"});
        }

        //return resolve with collection from mongo
        return new Promise(resolve => {
            MongoClient.connect(url, function(err, db) {
              if (err) throw err;
              var database = db.db('pwpg08');

              database.collection(collection).find().toArray(function(err, result) {
                if (err) throw err;
                db.close();
                resolve(result);
              });
              


            });
        });
    });
};




//insert into a database also protected by token
function insertDB(collection,body) {

    let token = body.token;
    let content = body.content;

    //console.log("token:")
    //console.log(token)
    //console.log("content:")
    //console.log(content)

    return jwt.verify(token, config.secret, function(err, decoded) {

        //return reject if token is invalid
        if (err) {
            console.log("error verifying token data");
            return Promise.reject({ "success": "false"});
        }


        return new Promise(resolve => {
            MongoClient.connect(url, function(err, db) {
                if (err) throw err;
                let database = db.db("pwpg08");

                  //console.log("insertDB");
                  //console.log(Array.isArray(content));


                  //in case of receiving array of orders (10 beer): insertMany into mongoDB 
                  if(Array.isArray(content)){
                    database.collection(collection).insertMany(content, function(err, result) {
                        //if (err) return res.status(500).send('Error on the server.');
                        if (err) throw err;
                        db.close();
                        resolve({ "success": "true"});
                    });
                  }
                  //otherwise if user the password has to be encrypted with bcrypt
                  else{
                    if(collection == "users"){
                      let hashedPassword = bcrypt.hashSync(content.password, 8);
                      content.password = hashedPassword;
                    }

                    database.collection(collection).insertOne(content, function(err, result) {
                    //if (err) return res.status(500).send('Error on the server.');
                    if (err) throw err;
                    db.close();
                    resolve({ "success": "true"});
                    });

                  }

            });
        });
    });
};

//update database (protected by token)
function updateDB(collection,body) {

    let token = body.token;
    let content = body.content;

    return jwt.verify(token, config.secret, function(err, decoded) {

        //return reject if token is invalid
        if (err) {
            console.log("error verifying token data");
            return Promise.reject({ "success": "false"});
        }

        //update db 
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, function(err, db) {
                if (err) throw err;
                let database = db.db("pwpg08");

                //users,products etc. are updated where _id matches the previous _id
                let id = new mongo.ObjectID(content._id);

                //in case of password update the password has to be hashed again
                if(collection == "users" && typeof content.password == "string"){
                  var hashedPassword = bcrypt.hashSync(content.password, 8);
                  content.password = hashedPassword;
                }

                var myquery = { "_id" : id }
                delete content._id;
                //previous data is overwritten with $set: content
                var newvalues = { $set: content };
                database.collection(collection).updateOne(myquery, newvalues, function(err, result) {
                    //if (err) return res.status(500).send('Error on the server.');
                    if (err) {throw err;}
                    db.close();
                    resolve({ "success": "true"});
                });
                
            });
        });
        
    });
};  

//outdated - just for archive
function hideDeleteMovedoutDB(collection,content) {
    return new Promise(resolve => {
        MongoClient.connect(url, function(err, db) {
            if (err) throw err;
            let database = db.db("pwpg08");

            let id = new mongo.ObjectID(content._id);
            //console.log(content);
            //console.log(content._id);
            //console.log(id);
            var myquery = { "_id" : id };
            delete content._id;

            var newvalues = "";


            //console.log("collection:");
            //console.log(collection);
            if(collection=="users"){
              newvalues = { $set: {"movedout" : "true"} };
            }
            else if(collection=="products"){
              newvalues = { $set: {"deleted" : "true"} };
            }
            else if(collection=="orders"){
              newvalues = { $set: {"deleted" : "true"} };
            }

            database.collection(collection).updateOne(myquery, newvalues, function(err, result) {
                //if (err) return res.status(500).send('Error on the server.');
                if (err) throw err;
                db.close();
                resolve({ "success": "true"});
            });
        });
    });
};


//handle normal file requests etc.
router.use(express.static(__dirname + "/"));


//outdated - just for archive
router.post('/register', function(req, res) {
  
  var hashedPassword = bcrypt.hashSync(req.body.password, 8);
  
  User.create({
    name : req.body.name,
    email : req.body.email,
    password : hashedPassword
  },
  function (err, user) {
    if (err) return res.status(200).send("There was a problem registering the user.")
    // create a token
    var token = jwt.sign({ id: user._id }, config.secret, {
      expiresIn: 86400 // expires in 24 hours
    });
    res.status(200).send({ auth: true, token: token });
  }); 
});



//decrypt token data 
router.get('/me/:token',  function(req, res) {
  //var token = req.headers['x-access-token'];

  var token = req.params.token;

  if (!token) return res.status(200).send({ auth: false, message: 'No token provided.' });
  
  jwt.verify(token, config.secret, async function(err, decoded) {
    if (err) return res.status(200).send({ auth: false, message: 'Failed to authenticate token.' });
    
    const json = await readDB("users",decoded.id);  
    res.send(json);

  });
});

//outdated - just for archive
router.use(function (user, req, res, next) {
  res.status(200).send(user);
});


//login endpoint
router.post('/login', function(req, res) {
  return new Promise((resolve, reject) => {
        MongoClient.connect(url, function(err, db) {
          if (err) throw err;
          var database = db.db("pwpg08");

          //console.log("req.body am Anfang");
          //console.log(req.body);
  
          //read user from collection by email adress
          return database.collection("users").find( JSON.parse(`{"email": "`+req.body.email+`" }`)).toArray(async function(err, result) {
            if (err) throw err;
            db.close();

            if(result.length == 0){
              return res.status(200).send({ auth: false, token: null, message: "item not found in database" });
            }

            //unauthorized user cannot access admin area (not admin or movedout)
            if((req.body.asAdmin == true && result[0].isAdmin == "0") || result[0].movedout == "1"){
              return res.status(200).send({ auth: false, token: null, message: "movedout 1"});
            }

     
            if (err) return res.status(500).send('Error on the server.');
            if (!result) return res.status(404).send('No user found.');


            if(typeof result[0] == 'undefined'){
              reject("missing password");
            }

            //compare password with database with compareSync
            var passwordIsValid = bcrypt.compareSync(req.body.password, result[0].password);
            

            if (!passwordIsValid) return res.status(200).send({ auth: false, token: null, message: "passwordIsInvalid" });

            //Create Token
            var token = jwt.sign({ id: result[0]._id }, config.secret, {
              expiresIn: 86400 // expires in 24 hours
            });

            //console.log(token);
            //console.log(result[0]);
            //console.log(req.body);

            //change notation from boolean to string (mongo format)
            req.body.asAdminLastLogin = req.body.asAdminLastLogin ? "1" : "0";

            //update last status of isAdminLastLogin for return of user
            await updateDB("users",{token: token, content: {"_id": result[0]._id, "isAdminLastLogin": req.body.asAdminLastLogin}});  
            
            res.status(200).send({ auth: true, token: token });

            resolve(result);
          });
          

        });
    });


});

router.get('/logout', function(req, res) {
  res.status(200).send({ auth: false, token: null });
}); 


router.get('/locations/:token', async function(req, res) {

    var token = req.params.token;

    if (!token) return res.status(400).send({ auth: false, message: 'No token provided.' });

    const json = await readDBToken("locations",token);  
    res.send(json);
});

router.get('/debug/locations', async function(req, res) {
    const json = await readDB("locations",null);  
    res.send(json);
});

router.get('/users/:token', async function(req, res) {
  
    var token = req.params.token;

    if (!token) return res.status(400).send({ auth: false, message: 'No token provided.' });

    const json = await readDBToken("users",token);  
    res.send(json);
});

router.get('/debug/users', async function(req, res) {
    const json = await readDB("users",null);  
    res.send(json);
});

router.post('/users/insert', async function(req, res) {
    const json = await insertDB("users",req.body);  
    res.send(json);
});
//,req.params.id
router.post('/users/update', async function(req, res) {
    const json = await updateDB("users",req.body);  
    res.send(json);
});

router.post('/users/movedout', async function(req, res) {
    const json = await hideDeleteMovedoutDB("users",req.body.content);  
    res.send(json);
});

router.get('/products/:token', async function(req, res) {

    var token = req.params.token;

    if (!token) return res.status(400).send({ auth: false, message: 'No token provided.' });

    const json = await readDBToken("products",token);  
    res.send(json);
});

router.get('/debug/products', async function(req, res) {
    const json = await readDB("products",null);  
    res.send(json);
});

router.post('/products/insert', async function(req, res) {
    const json = await insertDB("products",req.body);  
    res.send(json);
});

router.post('/products/update', async function(req, res) {
    const json = await updateDB("products",req.body);  
    res.send(json);
});

router.post('/products/hide', async function(req, res) {
    const json = await hideDeleteMovedoutDB("products",req.body.content);  
    res.send(json);
});

router.get('/orders/:token', async function(req, res) {

    var token = req.params.token;

    if (!token) return res.status(400).send({ auth: false, message: 'No token provided.' });

    const json = await readDBToken("orders",token);  
    res.send(json);
});

router.get('/debug/orders', async function(req, res) {
    const json = await readDB("orders",null);  
    res.send(json);
});

router.post('/orders/insert', async function(req, res) {
    const json = await insertDB("orders",req.body);  
    res.send(json);
});

router.post('/orders/delete', async function(req, res) {
    const json = await hideDeleteMovedoutDB("orders",req.body.content);  
    res.send(json);
});


module.exports = router;