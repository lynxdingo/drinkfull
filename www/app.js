var express = require('express');
var app = express();

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/');

//var UserController = require('./UserController');
//app.use('/users', UserController);

var AuthController = require('./AuthController');
//app.use('/api/auth', AuthController);

app.use('', AuthController);

module.exports = app;