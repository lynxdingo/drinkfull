import { OrderDatePipe } from './order-date';
import { Order } from '../../aids/order';

describe('OrderDatePipe', () => {
  // This pipe is a pure, stateless function so no need for BeforeEach
  let pipe = new OrderDatePipe();
  //mock orders
  let mockOrders: Order[] = [
    {
        _id: '',
    productId: '',
    locationId: '',
    userId: '',
    orderDate: 1547295420000,
    price: '',
    deleted: false,
    },
    {
        _id: '',
    productId: '',
    locationId: '',
    userId: '',
    orderDate: 1549098631000,
    price: '',
    deleted: false,
    },
    {
      _id: '',
  productId: '',
  locationId: '',
  userId: '',
  orderDate: 1517562600000,
  price: '',
  deleted: false,
  },
  ]

    //mock data information
    let mockMonth1: any = {
        id: '1',
        name: 'Februar',
        days: 28
    };
    let mockYear1: any = {
        id: '2019',
        name: '2019'
    };

    let mockMonth2: any = {
        id: '2',
        name: 'März',
        days: 31
    };
    let mockYear2: any = {
        id: '2017',
        name: '2017'
    };

    it('create an instance', () => {
        expect(pipe).toBeTruthy();
    });

    // to filter out the second order with mockMonth1 (Feburary) and mockYear1 (2019)
    it('only the second order corresponding to these month and year', () => {
        expect(pipe.transform(mockOrders, mockMonth1, mockYear1)).toEqual([
            mockOrders[1]
        ]);
    });

    //no order should be filtered out with the month corresponded(Feburary) only;
    it('there should be no orderd filtered out', () => {
        expect(pipe.transform(mockOrders, mockMonth1, mockYear2)).toEqual([]);
    });

    //no order should be filtered out with the the year corresponded(2019) only;
    it('there should be no orderd filtered out', () => {
        expect(pipe.transform(mockOrders, mockMonth2, mockYear1)).toEqual([]);
    });
});
