import { Pipe, PipeTransform } from '@angular/core';
import { Order } from '../../aids/order';

@Pipe({
    name: 'orderDate'
})
export class OrderDatePipe implements PipeTransform {
    /**
     * filter an array according to selectedMonth and selectedYear.
     */

    transform(value: Order[], month: any, year: any): Order[] {

        let filterMonth: number;
        let filterYear: number;
        let selectedMonth: number = parseInt(month.id, 10);
        let selectedYear: number = parseInt(year.id, 10);

        let filteredOrders: Order[] = [];

        for (let i = 0; i < value.length; i++) {
            let newDate = new Date(+value[i].orderDate);
            filterMonth = newDate.getMonth();
            filterYear = newDate.getFullYear();

            if (filterYear === selectedYear && filterMonth === selectedMonth) {
                //compare all the order items with selected year and month, and push into an empty array to store.
                filteredOrders.push(value[i]);
            }
        }
        return filteredOrders;

    }

}
