import { OrderLocationPipe } from './order-location';
import { Order } from '../../aids/order';
import { Location } from '../../aids/location';

describe('OrderLocationPipe', () => {
    // This pipe is a pure, stateless function so no need for BeforeEach
    let pipe = new OrderLocationPipe();
    let mockOrders: Order[] = [
        {
            _id: '',
            productId: '',
            locationId: '1212',
            userId: '',
            orderDate: '',
            price: '',
            deleted: false
        },
        {
            _id: '',
            productId: '',
            locationId: '1213',
            userId: '',
            orderDate: '',
            price: '',
            deleted: false
        }
    ];

    let mockLocation1: Location = {
        _id: '1212',
        locationName: 'haus1',
        imgSource: 'www.google.de'
    };

    let mockLocation2: Location = {
        _id: '1214',
        locationName: 'haus1',
        imgSource: 'www.google.de'
    };

    it('create an instance', () => {
        expect(pipe).toBeTruthy();
    });

    //to filter out the first order with mockLocation1
    it('there is only one order corresponding to this locationId', () => {
        expect(pipe.transform(mockOrders, mockLocation1)).toEqual([
            mockOrders[0]
        ]);
    });

    //there should be no order to be filtered with mockLocation2
    it('there is no order corresponding to this locationId', () => {
        expect(pipe.transform(mockOrders, mockLocation2)).toEqual([]);
    });
});
