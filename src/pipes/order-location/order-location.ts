import { Pipe, PipeTransform } from '@angular/core';
import { Order } from '../../aids/order';
import { Location } from '../../aids/location';

@Pipe({
    name: 'orderLocation'
})
export class OrderLocationPipe implements PipeTransform {
    /**
     * filter an array according to locationId
     */
    transform(value: Order[], location: Location): Order[] {
        let filteredOrders: Order[] = [];
        for (let i = 0; i < value.length; i++) {
            if (value[i].locationId === location._id) {
                //compare all the order items with selected locationId and push into an empty array to store.
                filteredOrders.push(value[i]);
            }
        }
        return filteredOrders;
    }
}
