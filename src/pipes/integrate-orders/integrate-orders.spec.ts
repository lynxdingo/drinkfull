import { IntegrateOrdersPipe } from './integrate-orders';
import { Order } from '../../aids/order';
import { Product } from '../../aids/product';

describe('IntegrateOrdersPipe', () => {
  // This pipe is a pure, stateless function so no need for BeforeEach
  let pipe = new IntegrateOrdersPipe();

  //mock order data, which should be integrated by mockProduct
  let mockOrders: Order[] = [
    {
        _id: '',
    productId: '1',
    locationId: '11',
    userId: '',
    orderDate: '',
    price: '1.00',
    deleted: false,
    },
    {
        _id: '',
    productId: '2',
    locationId: '12',
    userId: '',
    orderDate: '',
    price: '1.00',
    deleted: false,
    },
    {
      _id: '',
  productId: '1',
  locationId: '11',
  userId: '',
  orderDate: '',
  price: '1.00',
  deleted: false,
  },
  ]

  //mock different product data
  let mockProduct: Product[] = [
    {
      _id: '1',
      productName: 'A',
      locationId: '11',
      price: '2.00',
      count: 1,
      countMax: 1,
      dateAdded: 1,
      dateUpdated: 1,
      deleted: false,
      time: '',
      date: '',
      buyDrinkCount: 1,
  },
  {
    _id: '2',
    productName: 'B',
    locationId: '11',
    price: '1.00',
    count: 1,
    countMax: 1,
    dateAdded: 1,
    dateUpdated: 1,
    deleted: false,
    time: '',
    date: '',
    buyDrinkCount: 1,
},
  {
    _id: '2',
    productName: 'C',
    locationId: '12',
    price: '1.00',
    count: 1,
    countMax: 1,
    dateAdded: 1,
    dateUpdated: 1,
    deleted: false,
    time: '',
    date: '',
    buyDrinkCount: 1,
},
  ]
/*this result is expected as an output.
This result tests 3 main parts of the function:
1. the two orders with _id = '1' and locationId='11' should be combined into one group, the buyDrinkCount should be raisen to 2.
  And the order with _id = '2' and locationId='12' should be combined with buyDrinkCount = 1,
  And the item from mockProduct with _id = '2' and locationId='11' should be discarded.
2. Each item in result should be add a property productName as 'A' and 'C' seperately.
3. in item with _id = '1' and locationId='11', the price should be corrected as 2, (but not '1.00' or '2.00' or 1)
*/

  let result = [
    {
      locationId: '11',
      productId: '1',
      buyDrinkCount: 2,
      productName: 'A',
      price: 2,
  },
  {
    locationId: '12',
    productId: '2',
    buyDrinkCount: 1,
    productName: 'C',
    price: 1,
},
  ]

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('the example result should be shown', () => {
    expect(pipe.transform(mockOrders, mockProduct)).toEqual(result);
  });

});
