import { Pipe, PipeTransform } from '@angular/core';
import { Order } from '../../aids/order';
import { Product } from '../../aids/product';

@Pipe({
    name: 'integrateOrders'
})
export class IntegrateOrdersPipe implements PipeTransform {
    /**
     * take an unordered orders and combine all the items with same "productId" and "locationId" into one item and count the aomout these items.
     * by comparing the productId and locationId, add the corresponding productName and price to the integrated array and return it.
     */
    transform(orders: Order[], products: Product[]): any[] {
        let iOrders: any[] = []; //an array prepare to store the information of the different product items.
        let obj = {
            //an empty object to store relevant information of one specific product item.
            locationId: '',
            productId: '',
            buyDrinkCount: 0
        };

        if (orders.length > 0) {
            //comparing the "productId" and "locationId", try to find all the different product items and make count.
            for (let i = 0; i < orders.length; i++) {
                // inits values
                let locationId: string = orders[i].locationId;
                let productId: string = orders[i].productId;

                // gets the count of the same product item
                let count = orders.filter(
                    el =>
                        el.locationId === locationId &&
                        el.productId === productId
                ).length;

                // sets the found data to an object and pushes to local array
                obj = {
                    locationId: locationId,
                    productId: productId,
                    buyDrinkCount: count
                };
                iOrders.push(obj);

                // removing all unnessary orders with same productId and locationId from array (Keep only the item firstly appears).
                for (let j = i + 1; j < orders.length; ) {
                    if (
                        orders[j].locationId === locationId &&
                        orders[j].productId === productId
                    ) {
                        orders.splice(j, 1);
                    } else {
                        j++;
                    }
                }
            }

            // adds name and price to the orderObj
            for (let i = 0; i < products.length; i++) {
                let index = iOrders.findIndex(
                    el =>
                        el.productId === products[i]._id &&
                        el.locationId === products[i].locationId
                );
                if (index != -1) {
                    iOrders[index].productName = products[i].productName;
                    iOrders[index].price = parseFloat(products[i].price);
                }
            }
        }
        return iOrders;
    }
}
