import { NgModule } from '@angular/core';
import { OrderDatePipe } from './order-date/order-date';
import { IntegrateOrdersPipe } from './integrate-orders/integrate-orders';
import { OrderLocationPipe } from './order-location/order-location';
@NgModule({
	declarations: [OrderDatePipe,
    IntegrateOrdersPipe,
    OrderLocationPipe],
	imports: [],
	exports: [OrderDatePipe,
    IntegrateOrdersPipe,
    OrderLocationPipe]
})
export class PipesModule {}
