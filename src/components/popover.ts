import { Component } from '@angular/core';
import {  NavController, ViewController,NavParams } from 'ionic-angular';

@Component({
  template: `<img style="height: auto;" src="../g08/assets/imgs/{{imgSource}}.png">

  `
})

export class PopoverPage {

  imgSource: string;

  constructor(
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams,
  ) {
    this.imgSource= this.navParams.get('imgSource');


  }

  close() {
    this.viewCtrl.dismiss();
  }
}
