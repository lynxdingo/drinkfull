import { Component, Input } from '@angular/core';

@Component({
    selector: 'product-table',
    template: `
        <ion-list>
            <ion-row class="invoiceList" *ngFor="let drink of consumedList">
                <ion-col col-4>
                    <ion-icon [name]="drink.icon" item-start></ion-icon>
                    {{ drink.productName }}
                </ion-col>
                <ion-col col-2>
                    <span class="drinkPrice">{{ drink.price }}€</span>
                </ion-col>
                <ion-col col-2>
                    <span class="drinkTime">{{ drink.buyDrinkCount }}</span>
                    <span class="drinkTime">{{ drink.time }}</span>
                </ion-col>
                <ion-col class="consumDate" col-4>
                    <span *ngIf="drink.buyDrinkCount" class="drinkDate"
                        >{{ drink.price * drink.buyDrinkCount }}€</span
                    >
                    <span class="drinkDate">{{ drink.date }}</span>
                </ion-col>
            </ion-row>
        </ion-list>
        <ion-list class="consumSum">
            <ion-row>
                <ion-col col-4>
                    <span class="sumSpan">Monat Kosten:</span>
                </ion-col>
                <ion-col col-2> </ion-col>
                <ion-col col-2> </ion-col>
                <ion-col class="consumDate" col-4>
                    <span class="sumPreis">{{ totalMonthConsume }} €</span>
                </ion-col>
            </ion-row>
        </ion-list>
    `
})
export class productTable {
    @Input() consumedList: any;
}
