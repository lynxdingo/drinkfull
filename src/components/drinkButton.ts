import { Component, EventEmitter, Input, Output } from '@angular/core';
// import { OneDrink } from '../aids/drink';

@Component({
    selector: 'drink-button',
    template: `
        <button
            ion-button
            *ngIf="!isAdmin"
            [disabled]="drinkStatus.count == 0"
            color="primary"
            clear
            icon-only
            (click)="dec()"
        >
            <ion-icon name="md-remove-circle" is-active="false"></ion-icon>
        </button>

        <button
            ion-button
            *ngIf="isAdmin"
            color="primary"
            clear
            icon-only
            (click)="decAdmin()"
        >
            <ion-icon name="md-remove-circle" is-active="false"></ion-icon>
        </button>

        <button
            ion-button
            *ngIf="!isAdmin"
            [disabled]="drinkStatus.count == 0"
            color="primary"
            clear
            icon-only
            (click)="inc()"
        >
            <ion-icon name="md-add-circle" is-active="false"></ion-icon>
        </button>

        <button
            ion-button
            *ngIf="isAdmin"
            color="primary"
            clear
            icon-only
            (click)="incAdmin()"
        >
            <ion-icon name="md-add-circle" is-active="false"></ion-icon>
        </button>
    `
})
export class DrinkButtonComponent {

    @Input() drinkStatus: any;
    @Input() isAdmin: boolean;

    @Output() drinkStatusChanged = new EventEmitter();

    inc() {
        if (this.drinkStatus.buyDrinkCount < this.drinkStatus.count) {
            this.drinkStatus.buyDrinkCount++;
        } else {
            this.drinkStatus.buyDrinkCount = this.drinkStatus.count;
        }
        this.statusChanged(this.drinkStatus);
    }

    incAdmin() {
        let tMaxCount: number =
            this.drinkStatus.countMax - this.drinkStatus.count;
        if (this.drinkStatus.buyDrinkCount < tMaxCount) {
            this.drinkStatus.buyDrinkCount++;
        } else {
            this.drinkStatus.buyDrinkCount = tMaxCount;
        }
        this.statusChanged(this.drinkStatus);
    }

    dec() {
        if (this.drinkStatus.buyDrinkCount > 0) {
            this.drinkStatus.buyDrinkCount--;
        }
        this.statusChanged(this.drinkStatus);
    }

    decAdmin() {
        if (this.drinkStatus.buyDrinkCount + +this.drinkStatus.count > 0) {
            this.drinkStatus.buyDrinkCount--;
        }
        this.statusChanged(this.drinkStatus);
    }

    userInc(el) {
        if (el.buyDrinkCount < el.countMax) {
            el.buyDrinkCount++;
            this.statusChanged(el);
        } else {
            el.buyDrinkCount = el.countMax;
            this.statusChanged(el);
        }
    }

    userDec(el) {
        if (el.buyDrinkCount > 1) {
            el.buyDrinkCount--;
            this.statusChanged(el);
        } else {
            el.buyDrinkCount = 1;
            this.statusChanged(el);
        }
    }

    adminInc(el) {
        if (el.buyDrinkCount < el.countMax - el.countMax) {
            el.buyDrinkCount++;
            this.statusChanged(el);
        } else {
            el.buyDrinkCount = el.countMax - el.countMax;
            this.statusChanged(el);
        }
    }

    adminDec(el) {
        if (el.buyDrinkCount > 1) {
            el.buyDrinkCount--;
            this.statusChanged(el);
        } else {
            el.buyDrinkCount = 0;
            this.statusChanged(el);
        }
    }

    statusChanged(e) {
        this.drinkStatusChanged.emit(e);
    }
}
