import { Component, Input } from '@angular/core';

@Component({
  selector: 'drink-select',
  template: `<ion-select [(ngModel)]="drink">
      <ion-option value="wasser">Wasser</ion-option>
      <ion-option value="bier">Bier</ion-option>
      <ion-option value="kaffee">Kaffee</ion-option>
      <ion-option value="eis">Eis</ion-option>
      <ion-option value="wein">Wein</ion-option>
    </ion-select>`
})
export class DrinkSelectorComponent {
  @Input() drink: string;
}
