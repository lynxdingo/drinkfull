import { Component } from '@angular/core';
import {
    NavController,
    LoadingController,
    ToastController
} from 'ionic-angular';

import { MONTHS } from '../../aids/months';
import { YEARS } from '../../aids/years';
import { OrderService } from '../../services/orderService';
import { ProductService } from '../../services/productService';
import { LocationService } from '../../services/locationService';

//Pipes
import { OrderDatePipe } from '../../pipes/order-date/order-date';
import { IntegrateOrdersPipe } from '../../pipes/integrate-orders/integrate-orders';
import { Order } from '../../aids/order';
import { Product } from '../../aids/product';
import { Location } from '../../aids/location';

@Component({
    selector: 'page-banking',
    templateUrl: 'banking.html'
})
export class BankingPage {
    // holds the data of the house
    locationsList: any[] = [];

    // holds the data of the date
    chosenYear: string;
    chosenMonth: string;
    // local constants
    cMonths: any = MONTHS;
    cYears: any = YEARS;

    // holds data of consumption
    consumedDrinks: any[] = [];
    totalConsumed: number;

    // holds the products
    productsList: Product[] = [];

    constructor(
        public navCtrl: NavController,
        public loadingCtrl: LoadingController,
        public toastCtrl: ToastController,
        public orderSvc: OrderService,
        public productSvc: ProductService,
        public locationService: LocationService,
        private orderDatePipe: OrderDatePipe,
        private integrateOrdersPipe: IntegrateOrdersPipe
    ) {
        // --- Month Dropdown Setup --- //
        // MONTHS is an organized array ranging from 0 - 11
        this.chosenMonth = Object.assign(
            {},
            this.cMonths[new Date().getMonth()]
        );

        // --- Year Dropdown Setup ---//
        //Sets the current Year（2019） according to the format of YEAR
        this.chosenYear = Object.assign(
            {},
            this.cYears.find(
                el => el.id === new Date().getFullYear().toString()
            )
        );
    }

    ionViewWillEnter() {
        this.locationService.getLocations().then((locations: Location[]) => {
            this.productSvc.getProducts().then((products: Product[]) => {
                // inits the dropdown data
                this.locationsList = Object.assign([], locations);

                // inits products for checking purposes later
                this.productsList = Object.assign([], products);

                this.updatedFilteredOrders();
            });
        });
    }

    /**
     * Re-assigns the year
     * @param item selected month
     */
    monthSelected(item: any) {
        this.chosenMonth = Object.assign({}, item);
        this.updatedFilteredOrders();
    }
    /**
     * get and assign the "Year" selected by user
     * @param item yearSelected
     */
    yearSelected(item: any) {
        this.chosenYear = Object.assign({}, item);
        this.updatedFilteredOrders();
    }

    /**
     * Updates the consumedDrinks
     */
    updatedFilteredOrders(): void {
        this.orderSvc.getOrders().then((orders: Order[]) => {
            let tOrders = Object.assign([], orders);

            // at init consumedDrinks.length = 0
            if (tOrders.length != this.consumedDrinks.length) {
                let filteredByDate = this.orderDatePipe.transform(
                    tOrders,
                    this.chosenMonth,
                    this.chosenYear
                );
                let integratedOrderList = this.integrateOrdersPipe.transform(
                    filteredByDate,
                    this.productsList
                );

                // adds the locationName attribute to each element of the array
                this.consumedDrinks = integratedOrderList.map(el => ({
                    ...el,
                    locationName: this.locationsList.find(
                        loc => loc._id === el.locationId
                    ).locationName
                }));

                this.totalConsumed = this.sumPrice(this.consumedDrinks);
            }
        });
    }

    /**
     * Sums of the quantity of drinks
     * @param arr needs to have attribute 'price' and 'buyDrinkCount'
     */
    sumPrice(arr: any[]): number {
        if (arr.length != 0) {
            return arr
                .map(el => el.price * el.buyDrinkCount)
                .reduce((total, num) => total + num);
        }
    }
}
