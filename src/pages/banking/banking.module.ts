import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BankingPage } from './banking';
import { PipesModule } from '../../pipes/pipes.module'

@NgModule({
  declarations: [
    BankingPage,
    PipesModule

  ],
  imports: [
    IonicPageModule.forChild(BankingPage),
  ],
})
export class BankingPageModule {}
