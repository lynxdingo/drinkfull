import { Component } from '@angular/core';
import {
    NavController,
    LoadingController,
    ToastController,
    AlertController
} from 'ionic-angular';
import { App } from 'ionic-angular';
import { UserService } from '../../services/userService';
import { AuthService } from '../../services/authService';
import { CookieService } from 'ngx-cookie-service';
import { LoginPage } from '../login/login';
import { User } from '../../aids/user';

@Component({
    selector: 'page-adminAccount',
    templateUrl: 'adminAccount.html'
})
export class AdminAccountPage {
    constructor(
        public navCtrl: NavController,
        public loadingCtrl: LoadingController,
        public toastCtrl: ToastController,
        public userService: UserService,
        public authService: AuthService,
        private cookieService: CookieService,
        public appCtrl: App,
        public alerCtrl: AlertController
    ) {}

    cookieValue = 'UNKNOWN';

    oldUser: User = {
        _id: '',
        email: '',
        password: '',
        isAdmin: ''
    };

    newUser: User = {
        _id: '',
        email: '',
        password: '',
        isAdmin: ''
    };

    newPassword: { newPassword: string; newPasswordRepeat: string } = {
        newPassword: '',
        newPasswordRepeat: ''
    };

    ngOnInit(): void {
        this.cookieValue = this.cookieService.get('token');
    }

    async accountSetting() {
        //check if password fields empty
        if (
            this.oldUser.password == '' ||
            this.newPassword.newPassword == '' ||
            this.newPassword.newPasswordRepeat == ''
        ) {
            let toast = this.toastCtrl.create({
                message: 'One or more passwordfield(s) empty!',
                duration: 1200,
                position: 'top'
            });
            toast.present();
            return;
        }
        //compare new password fields for identity
        else if (
            this.newPassword.newPassword != this.newPassword.newPasswordRepeat
        ) {
            let toast = this.toastCtrl.create({
                message: 'Passwordfield not identical!',
                duration: 1200,
                position: 'top'
            });
            toast.present();
            return;
        }
        //compare old and new password for identity
        else if (this.oldUser.password == this.newPassword.newPassword) {
            let toast = this.toastCtrl.create({
                message: 'Old and new password identical!',
                duration: 1200,
                position: 'top'
            });
            toast.present();
            return;
        }

        //get old user data from token
        var nextStep = await this.userService
            .getUserFromToken()
            .then(user => {
                this.oldUser._id = user['_id'];
                this.oldUser.email = user['email'];
                this.oldUser.isAdmin = user['isAdmin'];

                return true;
            })
            .catch(reject => {
                let toast = this.toastCtrl.create({
                    message: 'Wrong Token Data!',
                    duration: 1200,
                    position: 'top'
                });
                toast.present();
                return false;
            });

        //check for success of previous step, otherwise return
        if (!nextStep) {
            return;
        }

        //verify oldUser password with login try
        nextStep = await this.authService
            .auth(this.oldUser)
            .then(resolve => {
                return true;
            })
            .catch(reject => {
                let toast = this.toastCtrl.create({
                    message: 'Wrong old password!',
                    duration: 1200,
                    position: 'top'
                });
                toast.present();
                return false;
            });

        //check for success of previous step, otherwise return
        if (!nextStep) {
            return;
        }

        //create newUser
        this.newUser._id = this.oldUser._id;
        this.newUser.email = this.oldUser.email;
        this.newUser.password = this.newPassword.newPassword;
        this.newUser.isAdmin = this.oldUser.isAdmin;

        //update User in mongoDB with newUser
        nextStep = await this.userService
            .updateUser(this.newUser)
            .then(resolve => {
                return true;
            })
            .catch(reject => {
                let toast = this.toastCtrl.create({
                    message: 'Could not update Data!',
                    duration: 1200,
                    position: 'top'
                });
                toast.present();
                return false;
            });

        //check for success of previous step, otherwise return
        if (!nextStep) {
            return;
        }

        let toast = this.toastCtrl.create({
            message: 'Updated password data!',
            duration: 1200,
            position: 'top'
        });
        toast.present();
    }

    async logout() {
        //Show logout confirmation and if success setRoot page to LoginPage and logout in authService
        let confirm = this.alerCtrl.create({
            title: `Logout Bestätigeung`,
            message: `Du wirst ausloggen, klicke auf <span class="logoutMessage">BESTÄTIGEN </span> um fortzufahren`,
            cssClass: 'alertCustomCss',
            buttons: [
                {
                    text: 'Abbrechen',
                    handler: () => {}
                },
                {
                    text: 'Bestätigen',
                    handler: async () => {
                        this.appCtrl.getRootNav().setRoot(LoginPage);

                        this.authService.logout();
                        let toast = this.toastCtrl.create({
                            message: 'Logout successful',
                            duration: 2000,
                            position: 'top'
                        });
                        toast.present();
                    }
                }
            ]
        });
        confirm.present();
    }
}
