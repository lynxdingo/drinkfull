import { Component } from '@angular/core';
import {
    NavController,
    AlertController,
    LoadingController,
    ToastController,
    PopoverController
} from 'ionic-angular';
import { PopoverPage } from '../../components/popover';
import { LocationService } from '../../services/locationService';
import { ProductService } from '../../services/productService';
import { OrderService } from '../../services/orderService';
import { Product } from '../../aids/product';
import { Location } from '../../aids/location';

@Component({
    selector: 'page-lobby',
    templateUrl: 'lobby.html'
})
export class LobbyPage {
    // holds the data of the house
    selectedLocation: any = {};
    locationDropdown: any[] = [];

    constructor(
        public navCtrl: NavController,
        public alerCtrl: AlertController,
        public loadingCtrl: LoadingController,
        public toastCtrl: ToastController,
        public popoverCtrl: PopoverController,
        public locationService: LocationService,
        public productSvc: ProductService,
        public orderSvc: OrderService
    ) {}

    ngOnInit() {
        this.locationService.getLocations().then((locations: Location[]) => {
            let tArray: any[] = [];
            locations.forEach((el: any) => {
                // inits the data for the dropdown
                tArray.push({
                    _id: el._id,
                    locationName: el.locationName,
                    drinks: this.assignDrinks(el._id)
                });
            });

            // inits the dropdown data
            this.locationDropdown = tArray;

            // inits the presenting data of the page
            this.selectedLocation = Object.assign({}, tArray[0]);
        });
    }

    /**
     * Sets the data to the selectedLocation
     * @param item
     */
    selectDropdownLocation(item: any): void {
        this.selectedLocation = Object.assign({}, item);
    }

    /**
     * Assigns the drinks to the location
     * @param locationId
     */
    assignDrinks(locationId: string): Promise<Product[]> {
        return new Promise((resolve, reject) => {
            this.productSvc.getProducts().then((products: Product[]) => {
                let filteredProducts: Product[] = products
                    .filter(el => el.locationId === locationId)
                    .map(el => {
                        // adds the buyDrinkCount attribute to the productsObject
                        return Object.assign({ buyDrinkCount: 1 }, el);
                    });
                if (filteredProducts.length == 0 || !products) {
                    reject([]);
                }
                resolve(filteredProducts);
            });
        });
    }

    /**
     * Confirms the purchase
     * @param drink
     */
    confirmPurchase(drink: Product): void {
        let confirm = this.alerCtrl.create({
            title: `Deine Bestellung`,
            message: `${drink.buyDrinkCount} ${
                drink.productName
            } für ${+drink.price * drink.buyDrinkCount}€?`,
            buttons: [
                {
                    text: 'Abbrechen',
                    handler: () => {}
                },
                {
                    text: 'Bestätigen',
                    handler: () => {
                        this.acceptConfirmPurchase(
                            drink,
                            this.selectedLocation._id
                        );
                    }
                }
            ]
        });
        confirm.present();
    }

    /**
     * Creates orders and updates products.
     * @param drink
     * @param selectedLocationId
     */
    async acceptConfirmPurchase(drink: Product, selectedLocationId: string) {
        // Creates an order
        let hasBeenCreated: boolean = await this.orderSvc.createOrder(
            drink,
            drink.buyDrinkCount,
            selectedLocationId
        );
        // Updates the product only if order has been created
        if (hasBeenCreated) {
            // reduces the (countMax - count) amount by the bought quantity
            drink.count = +drink.count - drink.buyDrinkCount;

            let hasBeenUpdated: boolean = await this.updateProductCount(drink);
            // Resets the count only if product has been updated
            if (hasBeenUpdated) {
                // resets buyDrinkCount
                drink.buyDrinkCount = 1;
            }
        }
    }

    /**
     * Updates the count of the product
     * @param product
     */
    updateProductCount(product: Product): Promise<boolean> {
        return this.productSvc
            .updateProduct(product)
            .then((isUpdated: boolean) => {
                return isUpdated;
            });
    }

    /**
     * Presents the location image
     * @param imgSource the image needs to be name as the locationId
     */
    presentPopover(imgSource: string): void {
        let popover = this.popoverCtrl.create(PopoverPage, {
            imgSource: imgSource
        });
        popover.present();
    }
}
