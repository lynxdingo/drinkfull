import { async, TestBed } from '@angular/core/testing';
import {
    IonicModule,
    NavController,
    AlertController,
    LoadingController,
    ToastController,
    PopoverController
} from 'ionic-angular';

import { LobbyPage } from './lobby';

import {
    LocationServiceMock,
    ProductServiceMock,
    OrderServiceMock,
    ToastControllerMock,
    LoadingControllerMock,
    AlertControllerMock,
    NavControllerMock,
    PopoverControllerMock
} from '../../../test-config/mocks-ionic';

import { LocationService } from '../../services/locationService';
import { ProductService } from '../../services/productService';
import { OrderService } from '../../services/orderService';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Product } from '../../aids/product';

xdescribe('Lobby Page', () => {
    let fixture;
    let component;

    // Spy Ons
    let orderSvcSpy: jasmine.SpyObj<OrderService>;

    // Constructor Variables
    let navCtrl,
        alertCtrl,
        loadingCtrl,
        toastCtrl,
        popoverCtrl,
        locationSvc,
        productSvc,
        orderSvc;

    // Controller
    let ctrl;

    // TODO: look at account.spec.ts and adjust the two next beforeEach functions
    beforeEach(async(() => {
        const spyThis = jasmine.createSpyObj('this', ['updateProductCount']);
        const spyOrderSvc = jasmine.createSpyObj('OrderService', [
            'createOrder'
        ]);

        TestBed.configureTestingModule({
            declarations: [LobbyPage],
            imports: [],
            providers: [
                { provide: NavController, useClass: NavControllerMock },
                { provide: AlertController, useClass: AlertControllerMock },
                { provide: LoadingController, useClass: LoadingControllerMock },
                { provide: ToastController, useClass: ToastControllerMock },
                { provide: PopoverController, useClass: PopoverControllerMock },
                { provide: LocationService, useClass: LocationServiceMock },
                { provide: ProductService, useClass: ProductServiceMock },
                { provide: OrderService, useValue: spyOrderSvc }
            ],
            schemas: [NO_ERRORS_SCHEMA]
        });
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LobbyPage);
        component = fixture.componentInstance;

        // Constructor Variables
        navCtrl = TestBed.get('NavController');
        alertCtrl = TestBed.get('AlertController');
        loadingCtrl = TestBed.get('LoadingController');
        toastCtrl = TestBed.get('ToastController');
        popoverCtrl = TestBed.get('PopoverController');
        locationSvc = TestBed.get('LocationService');
        productSvc = TestBed.get('ProductService');
        orderSvc = TestBed.get('OrderService');

        // Controller
        ctrl = new LobbyPage(
            navCtrl,
            alertCtrl,
            loadingCtrl,
            toastCtrl,
            popoverCtrl,
            locationSvc,
            productSvc,
            orderSvc
        );
    });

    describe('#acceptConfirmPurchase', () => {
        let drink: Product = {
            count: 2,
            countMax: 3,
            dateAdded: 5,
            dateUpdated: 6,
            deleted: '0',
            locationId: '1',
            price: '10',
            productName: 'drink'
        };
        let locationId = '';

        it('nothing should happen because not equal location', () => {
            const stubValue = false;

            ctrl.acceptConfirmPurchase(drink, location);
            expect(orderSvc.createOrder());
        });
    });
});
