import { Component } from '@angular/core';
import {
    NavController,
    AlertController,
    ToastController,
    LoadingController
} from 'ionic-angular';
import { UserService } from '../../services/userService';
import { LocationService } from '../../services/locationService';
import { User } from '../../aids/user';

interface OnInit {
    ngOnInit(): void;
}

@Component({
    selector: 'page-users',
    templateUrl: 'users.html'
})
export class UsersPage implements OnInit {
    selectedLocation: any;
    locationDropdown: any[];
    locations: any[];
    users: any[];
    newUserIsAdmin: boolean = false;
    checkBoxUserIsAdmin: boolean[] = [];
    checkBoxUserMovedOut: boolean[] = [];
    UserLocations: String[] = [];

    newUser: User = {
        _id: null,
        email: null,
        password: 'abc',
        countDonations: 0,
        monthlyLimit: 50,
        movedout: '0',
        isAdmin: '0'
    };

    constructor(
        public navCtrl: NavController,
        public alerCtrl: AlertController,
        public toastCtrl: ToastController,
        public loadingCtrl: LoadingController,
        public userService: UserService,
        public locationService: LocationService
    ) {}

    async ngOnInit(): Promise<void> {
        //read Users from DB for table
        await this.readUsers();
        //read Locations from DB for table
        await this.readLocations();

        //convert string array of admin/movedOut status to temp array
        for (let i = 0; i < this.users.length; i++) {
            this.checkBoxUserIsAdmin.push(
                this.users[i].isAdmin == '1' ? true : false
            );

            this.checkBoxUserMovedOut.push(
                this.users[i].movedout == '1' ? true : false
            );
        }

        //convert Location _id array to temp Location name array
        this.users.forEach(user => {
            this.UserLocations.push(
                this.readLocationNameFromLocationId(user.locationid)
            );
        });
    }

    //read locations from mongoDB into temp Array
    async readLocations() {
        this.locations = await this.locationService
            .getLocations()
            .then(resolve => {
                return resolve;
            })
            .catch(reject => {
                return [];
            });
    }

    //convert location _id to name
    readLocationNameFromLocationId(_id: String): String {
        let foundLocation = this.locations.find(
            location => location._id == _id
        );
        if (!foundLocation) {
            return 'no location';
        } else {
            return foundLocation.locationName;
        }
    }

    //read Users from mongoDB to users array
    async readUsers() {
        this.users = await this.userService
            .getUsers()
            .then(resolve => {
                return resolve;
            })
            .catch(reject => {
                return [];
            });
    }

    //update user in mongoDB
    async setUser(user: User, isAdminChecked: number, removedChecked: number) {
        //convert boolean of checkboxes to String for mongo
        if (isAdminChecked) {
            user.isAdmin = '1';
        } else {
            user.isAdmin = '0';
        }

        if (removedChecked) {
            user.movedout = '1';
        } else {
            user.movedout = '0';
        }

        //deleted password so it is not overwritten by the encrypted one
        delete user.password;

        //update user Data in mongoDB
        var nextStep = await this.userService
            .updateUser(user)
            .then(resolve => {
                return true;
            })
            .catch(reject => {
                let toast = this.toastCtrl.create({
                    message: 'Could not update Data!',
                    duration: 1200,
                    position: 'top'
                });
                toast.present();
                return false;
            });

        //check for success of previous step, otherwise return
        if (!nextStep) {
            return;
        }

        //read Users from mongoDB to users array
        await this.readUsers();

        let toast = this.toastCtrl.create({
            message: 'Updated User data!',
            duration: 1200,
            position: 'top'
        });
        toast.present();
    }

    //add new user in mongoDB
    async addUser() {
        //save current time for dateAdded
        this.newUser.dateAdded = new Date().getTime();

        //convert checkbox to string
        if (this.newUserIsAdmin == true) {
            this.newUser.isAdmin = '1';
        } else {
            this.newUser.isAdmin = '0';
        }

        //check if input fields are empty otherwise return
        if (
            this.newUser.firstname == '' ||
            this.newUser.lastname == '' ||
            this.newUser.email == '' ||
            this.newUser.locationid == '' ||
            this.newUser.roomid == ''
        ) {
            return;
        }

        //insert new User into database with service
        var nextStep = await this.userService
            .insertUser(this.newUser)
            .then(resolve => {
                let toast = this.toastCtrl.create({
                    message: 'Inserted new user!',
                    duration: 1200,
                    position: 'top'
                });
                toast.present();
                return true;
            })
            .catch(reject => {
                let toast = this.toastCtrl.create({
                    message: 'Could not insert new user!',
                    duration: 1200,
                    position: 'top'
                });
                toast.present();
                return false;
            });

        if (!nextStep) {
            return;
        }

        //read Users from mongoDB to users array
        await this.readUsers();

        //add checkbox values from string to boolean
        this.checkBoxUserIsAdmin.push(
            this.newUser.isAdmin == '1' ? true : false
        );
        this.checkBoxUserMovedOut.push(false);

        //add location name next to user name
        this.UserLocations.push(
            this.readLocationNameFromLocationId(this.newUser.locationid)
        );
    }
}
