import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { invoiceSegmentPage } from './invoiceSegment';
import { PipesModule } from '../../../pipes/pipes.module'

@NgModule({
  declarations: [
    invoiceSegmentPage,
    PipesModule

  ],
  imports: [
    IonicPageModule.forChild(invoiceSegmentPage),
  ],
})
export class invoiceSegmentModule {}
