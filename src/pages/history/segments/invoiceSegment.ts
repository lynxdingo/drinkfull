import { Component } from '@angular/core';
import {
    NavController,
    LoadingController,
    ToastController
} from 'ionic-angular';

import { MONTHS } from '../../../aids/months';
import { YEARS } from '../../../aids/years';
import { OrderService } from '../../../services/orderService';
import { ProductService } from '../../../services/productService';
import { LocationService } from '../../../services/locationService';

//Pipes
import { OrderDatePipe } from '../../../pipes/order-date/order-date';
import { IntegrateOrdersPipe } from '../../../pipes/integrate-orders/integrate-orders';
import { Order } from '../../../aids/order';
import { Product } from '../../../aids/product';
import { Location } from '../../../aids/location';

import { UserService } from '../../../services/userService';

import jsPDF from 'jspdf';
import 'jspdf-autotable';
import { User } from '../../../aids/user';

@Component({
    selector: 'invoice-segment',
    templateUrl: 'invoiceSegment.html'
})
export class invoiceSegmentPage {
    // holds the data of the house
    locationsList: any[] = [];

    // holds the data of the date
    chosenYear: any;
    chosenMonth: any;
    // local constants
    cMonths: any = MONTHS;
    cYears: any = YEARS;

    // holds data of consumption
    consumedDrinks: any[] = [];
    totalConsumed: number = 0;
    monthlyLimit: number = 0;

    // holds the products
    productsList: Product[] = [];

    myData: User;

    constructor(
        public navCtrl: NavController,
        public loadingCtrl: LoadingController,
        public toastCtrl: ToastController,
        public orderSvc: OrderService,
        public productSvc: ProductService,
        public locationService: LocationService,
        private orderDatePipe: OrderDatePipe,
        private integrateOrdersPipe: IntegrateOrdersPipe,
        public userSvc: UserService
    ) {
        // --- Month Dropdown Setup --- //
        // MONTHS is an organized array ranging from 0 - 11
        this.chosenMonth = Object.assign(
            {},
            this.cMonths[new Date().getMonth()]
        );

        // --- Year Dropdown Setup ---//
        //Sets the current Year（2019） according to the format of YEAR
        this.chosenYear = Object.assign(
            {},
            this.cYears.find(
                el => el.id === new Date().getFullYear().toString()
            )
        );
    }

    // change to ngDoCheck() to live reload
    async ionViewWillEnter() {
        this.myData = await this.userSvc.getUserFromToken();

        this.locationService.getLocations().then((locations: Location[]) => {
            this.productSvc.getProducts().then((products: Product[]) => {
                // inits the dropdown data
                this.locationsList = Object.assign([], locations);

                // inits products for checking purposes later
                this.productsList = Object.assign([], products);

                this.updatedFilteredOrders();
            });
        });
    }

    /**
     * Re-assigns the year
     * @param item selected month
     */
    monthSelected(item: any) {
        this.chosenMonth = Object.assign({}, item);
        this.updatedFilteredOrders();
    }
    /**
     * get and assign the "Year" selected by user
     * @param item yearSelected
     */
    yearSelected(item: any) {
        this.chosenYear = Object.assign({}, item);
        this.updatedFilteredOrders();
    }

    /**
     * Updates the consumedDrinks
     */
    updatedFilteredOrders(): void {
        this.orderSvc.getOrders().then(async (orders: Order[]) => {
            let tOrders = Object.assign([], orders);
            let tOrdersNew = [];
            let userId: String = '';

            userId = await this.userSvc.getUserFromToken().then(user => {
                return user['_id'];
            });

            //only read orders that have userId
            for (var i = 0; i < tOrders.length; i++) {
                if (userId == tOrders[i].userId) {
                    tOrdersNew.push(tOrders[i]);
                }
            }

            tOrders = tOrdersNew;

            // at init consumedDrinks.length = 0
            if (tOrders.length != this.consumedDrinks.length) {
                let filteredByDate = this.orderDatePipe.transform(
                    tOrders,
                    this.chosenMonth,
                    this.chosenYear
                );
                let integratedOrderList = this.integrateOrdersPipe.transform(
                    filteredByDate,
                    this.productsList
                );

                // adds the locationName attribute to each element of the array
                integratedOrderList = integratedOrderList.map(el => ({
                    ...el,
                    locationName: this.locationsList.find(
                        loc => loc._id === el.locationId
                    ).locationName
                }));

                this.consumedDrinks = integratedOrderList;
                this.totalConsumed = this.sumPrice(this.consumedDrinks);
                this.monthlyLimit =
                    this.myData.monthlyLimit - this.totalConsumed;
            }
        });
    }

    /**
     * Sums of the quantity of drinks
     * @param arr needs to have attribute 'price' and 'buyDrinkCount'
     */
    sumPrice(arr: any[]): number {
        if (arr.length != 0) {
            return arr
                .map(el => el.price * el.buyDrinkCount)
                .reduce((total, num) => total + num);
        }
    }

    //function displays a prompt message for sending invoice.
    async download(): Promise<void> {
        let user: any[] = [];
        let locations: any[] = [];
        let location: string = '';
        let date = new Date();
        let body: any[] = [];

        //get userdata from token for invoice filling
        user = await this.userSvc
            .getUserFromToken()
            .then(user => {
                return [user];
            })
            .catch(reject => {
                return [];
            });
        
        //get locations data from token for invoice filling
        locations = await this.locationService
            .getLocations()
            .then(resolve => {
                return resolve;
            })
            .catch(reject => {
                return [];
            });

        //assign locationName to locationid
        location = locations.filter(function(item) {
            return item._id === user[0].locationid;
        })[0].locationName;

        //fill body for table with consumedDrinks
        for (let i = 0; i < this.consumedDrinks.length; i++) {
            body.push([
                i + 1,
                this.consumedDrinks[i].productName,
                this.consumedDrinks[i].locationName,
                this.consumedDrinks[i].buyDrinkCount,
                this.consumedDrinks[i].price
            ]);
        }

        //create jsPDF
        const doc = new jsPDF();
        

        //caption of invoice
        doc.setFontSize(24);
        doc.text(
            25,
            25,
            'Rechnung - ' +
                this.chosenMonth.name +
                ' ' +
                this.chosenYear.name +
                ' - Wohnheim XY'
        );
        
        //left field of address
        doc.setFontSize(15);
        doc.text(20, 45, 'Wohnheim XY\nBeispielstraße 1\n80000 München');
        
        //right field of address filled with user data
        doc.text(
            110,
            45,
            user[0].firstname +
                ' ' +
                user[0].lastname +
                '\nWohnheim XY\n' +
                location +
                '/Raum ' +
                user[0].roomid +
                '\nBeispielstraße 1\n80000 München'
        );

        //invoice number
        doc.text(
            25,
            85,
            'Rechnung Nr.: ' +
                this.chosenYear.id +
                '000' +
                (this.chosenMonth.id + 1)
        );
        
        //invoice date
        doc.text(
            135,
            85,
            'Datum: ' +
                date.getDate() +
                '.' +
                (date.getMonth() + 1) +
                '.' +
                date.getFullYear()
        );

        //table of contentss
        // let head = [['ID', 'Getränk', 'Haus', 'Anzahl', 'Preis in €']];
        // doc.autoTable({ head: head, body: body, startY: 90 });
        doc.line(
            20,
            99 + this.consumedDrinks.length * 8,
            190,
            99 + this.consumedDrinks.length * 8
        );

        //total of invoice in €
        doc.text(
            130,
            105 + this.consumedDrinks.length * 8,
            'Gesamtbetrag: ' + this.totalConsumed + '€'
        );
        doc.output('dataurlnewwindow');

        // Save the PDF
        doc.save(`Rechnung_${date.getFullYear()}_${date.getMonth() + 1}.pdf`);

        // Loading spinner
        this.loadingCtrl
            .create({
                content: 'Please wait...',
                duration: 500,
                dismissOnPageChange: true
            })
            .present();

        setTimeout(function() {
            // Toast the top
            let toast = this.toastCtrl.create({
                message: 'Rechnung steht zum download bereit!',
                duration: 2000,
                position: 'top'
            });

            toast.present(toast);
        }, 1000);
    }
}
