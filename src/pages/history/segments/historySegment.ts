import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { OrderService } from '../../../services/orderService';
import { ProductService } from '../../../services/productService';
import { Product } from '../../../aids/product';
import { Order } from '../../../aids/order';
import { UserService } from '../../../services/userService';
import { User } from '../../../aids/user';

@Component({
    selector: 'history-segment',
    templateUrl: 'historySegment.html'
})
export class historySegment {
    // module to hold start and end date
    event: any = {
        start: '',
        end: ''
    };

    consumedDrinks: any = {}; // as object to put drinks into async
    totalMonthConsume: number;

    myData: User;

    constructor(
        public navCtrl: NavController,
        public loadingCtrl: LoadingController,
        public orderSvc: OrderService,
        public productSvc: ProductService,
        public userSvc: UserService
    ) {}

    // called only once, on init
    async ionViewDidLoad() {
        // setting up the dates for this month
        this.setThisMonth();

        this.myData = await this.userSvc.getUserFromToken();

        if (this.myData) {
            this.getMyHistory();
        }
    }

    // called everytime that will enter
    ionViewWillEnter() {
        if (this.myData) {
            this.getMyHistory();
        }
    }

    /**
     * Gets all the products I have purchased.
     */
    getMyHistory() {
        this.productSvc.getProducts().then((products: Product[]) => {
            this.orderSvc.getOrders().then((orders: Order[]) => {
                let result = new Promise((resolve, reject) => {
                    resolve(
                        this.getProductInformation(
                            this.myData._id,
                            orders,
                            products
                        )
                    );
                });

                // sets to the visible products
                this.consumedDrinks.orders = result;
            });
        });
    }

    /**
     * Gets more information for the products based off of the user.
     * @param userId
     * @param orders
     * @param products
     */
    getProductInformation(
        userId: string,
        orders: Order[],
        products: Product[]
    ): Product[] {
        let productsInfo: Product[] = [];
        // filters over userId
        orders = orders.filter(el => el.userId === userId);
        orders.forEach((order: Order) => {
            // getting more information on the product
            let prodInfo: Product = this.productSvc.getProductInformation(
                order,
                products
            );

            if (prodInfo) {
                productsInfo.push(prodInfo);
            }
        });

        // filters the products within the time frame
        productsInfo = this.filterProductsByTime(this.event, productsInfo);

        // sums up the total of all products consumed
        this.sumTotal(productsInfo);

        return productsInfo;
    }

    /**
     * Filters the product list based off of the time interval
     * @param timeInterval {start:Date,end:Date}
     * @param products products within the time interval
     */
    filterProductsByTime(timeInterval: any, products: Product[]): Product[] {
        let minDate = new Date(timeInterval.start);
        let maxDate = new Date(timeInterval.end);

        let filteredProducts: Product[] = products.filter(
            el => minDate <= new Date(el.date) && new Date(el.date) <= maxDate
        );

        return filteredProducts;
    }

    /**
     * Filters the orders when ion-datetime has been changed.
     */
    filterByDate() {
        // checks if start-date larger than end-date, then set equal
        if (new Date(this.event.start) >= new Date(this.event.end)) {
            this.event.end = this.event.start;
        }
        // checks if end-date smaller than start-date, then set equal
        if (new Date(this.event.end) <= new Date(this.event.start)) {
            this.event.start = this.event.end;
        }

        this.getMyHistory();
    }

    /**
     * Sets date range to this month
     */
    setThisMonth(): void {
        let firstDay: Date = new Date();
        firstDay.setDate(1); // first day of this month
        let lastDay: Date = new Date();
        lastDay.setDate(0); // last day of previous month
        lastDay.setMonth(lastDay.getMonth() + 1);
        this.event.start = firstDay.toISOString();
        this.event.end = lastDay.toISOString();
    }

    /**
     * Sums of the total products consumed
     * @param arr
     */
    sumTotal(arr: Product[]): void {
        this.totalMonthConsume = 0;
        arr.forEach(el => {
            this.totalMonthConsume += +el.price;
        });
    }
}
