import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';

@Component({
    selector: 'page-history',
    templateUrl: 'history.html'
})
export class HistoryPage {
    segmentList: string = 'history';

    constructor(
        public navCtrl: NavController,
        public loadingCtrl: LoadingController
    ) {}

    ionViewDidLoad() {}
}
