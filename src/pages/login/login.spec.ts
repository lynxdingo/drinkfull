import { async, TestBed } from '@angular/core/testing';
import {
    IonicModule,
    NavController,
    NavParams,
    ToastController
} from 'ionic-angular';

import { LoginPage } from './login';
import {
    NavControllerMock,
    NavParamsMock,
    UserServiceMock,
    CookieServiceMock,
    ToastControllerMock,
    AuthServiceMock
} from '../../../test-config/mocks-ionic';
import { AuthService } from '../../services/authService';
import { CookieService } from 'ngx-cookie-service';
import { UserService } from '../../services/userService';

import { TabsPage } from '../tabs/tabs';

xdescribe('Login Page', () => {
  let fixture;
  let component;

  let cookieSvc: jasmine.SpyObj<CookieService>;

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginPage);
    component = fixture.componentInstance;
    const spy = jasmine.createSpyObj('CookieService', ['get']);

    TestBed.configureTestingModule({
        declarations: [LoginPage, TabsPage],
        imports: [],
        providers: [
            {provide: CookieService, useValue: spy},
            { provide: NavController, useClass: NavControllerMock },
            { provide: ToastController, useClass: ToastControllerMock },
            { provide: AuthService, useClass: AuthServiceMock },
            { provide: UserService, useClass: UserServiceMock },
            { provide: NavParams, useClass: NavParamsMock },
        ]
    });
    cookieSvc = TestBed.get(CookieService);
  });

    beforeEach(async(() => {
        
    }));

    describe('#ionViewWillEnter', () => {
        it('should use CookieService', () => {
            
            expect(cookieSvc.get()).toBe('token');
          });
        
        // TODO: create test
    });
});
