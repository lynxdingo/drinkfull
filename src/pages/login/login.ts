import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { AuthService } from '../../services/authService';

import { TabsPage } from '../tabs/tabs';

import { CookieService } from 'ngx-cookie-service';

import { UserService } from '../../services/userService';

@Component({
    selector: 'page-login',
    templateUrl: 'login.html'
})
export class LoginPage {
    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public authService: AuthService,
        public toastCtrl: ToastController,
        private cookieService: CookieService,
        public userService: UserService
    ) {}

    user: {
        email: string;
        password: string;
        asAdmin: boolean;
        asAdminLastLogin: boolean;
    } = {
        email: '',
        password: '',
        asAdmin: false,
        asAdminLastLogin: false
    };

    cookieValue: string = '';

    // start with TabPage and with 'asAdmin'
    doLogin() {
        this.navCtrl.push(TabsPage, {
            asAdmin: this.user.asAdmin
        });
        let toast = this.toastCtrl.create({
            message: 'Login successfully!',
            duration: 1200,
            position: 'top'
        });
        toast.present();
    }

    async ionViewWillEnter() {
        this.cookieValue = this.cookieService.get('token');

        if (this.cookieValue != '') {
            //get User data with token, write date to User
            await this.userService
                .getUserFromToken()
                .then(user => {
                    this.navCtrl.push(TabsPage, {
                        asAdmin: user['isAdminLastLogin'] == '1' ? true : false
                    });
                    return true;
                })
                .catch(reject => {
                    let toast = this.toastCtrl.create({
                        message: 'Wrong Token Data!',
                        duration: 1200,
                        position: 'top'
                    });
                    toast.present();
                    return false;
                });
        }
    }

    async authUser() {
        this.user.asAdminLastLogin = this.user.asAdmin;
        this.authService
            .auth(this.user)
            .then(() => {
                this.doLogin();
            })
            .catch(() => {
                let toast = this.toastCtrl.create({
                    message: 'Wrong Login Data!',
                    duration: 1200,
                    position: 'top'
                });
                toast.present();
            });
    }
}
