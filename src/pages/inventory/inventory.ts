import { Component } from '@angular/core';
import {
    NavController,
    AlertController,
    LoadingController,
    ToastController
} from 'ionic-angular';
import { LocationService } from '../../services/locationService';
import { ProductService } from '../../services/productService';
import { OrderService } from '../../services/orderService';
import { Product } from '../../aids/product';
import { Location } from '../../aids/location';

@Component({
    selector: 'page-inventory',
    templateUrl: 'inventory.html'
})
export class InventoryPage {
    //holds data of selected house
    selectedLocation: any = {};
    //holds data of all houses
    locationDropdown: any[];

    //array of locations
    locationsList: Location[];

    isAdmin: boolean = true;

    editedProduct: Product = {
        _id: '',
        productName: '',
        locationId: '',
        price: '',
        count: '',
        countMax: '',
        dateAdded: '',
        dateUpdated: '',
        deleted: '0'
    };

    newProduct: Product = {
        productName: '',
        locationId: '',
        price: '',
        count: '',
        countMax: '',
        dateAdded: new Date().getTime().toString(),
        dateUpdated: new Date().getTime().toString(),
        deleted: '0'
    };

    constructor(
        public navCtrl: NavController,
        public alerCtrl: AlertController,
        public loadingCtrl: LoadingController,
        public toastCtrl: ToastController,
        public locationService: LocationService,
        public productService: ProductService,
        public orderSvc: OrderService
    ) {}

    async ngOnInit(): Promise<void> {

        //read locations for drop down menue to locationlist
        await this.readLocations();

        //read locations and convert to tArray
        this.locationService.getLocations().then((locations: Location[]) => {
            let tArray: any[] = [];
            locations.forEach((el: Location) => {
                // inits the data for the dropdown
                tArray.push({
                    _id: el._id,
                    locationName: el.locationName,
                    drinks: this.assignDrinks(el._id)
                });
            });

            // inits the dropdown data
            this.locationDropdown = tArray;

            // inits the presenting data of the page
            this.selectedLocation = Object.assign({}, tArray[0]);
        });
    }

    //read locations to locationslist
    async readLocations() {
        this.locationsList = await this.locationService
            .getLocations()
            .then(resolve => {
                return resolve;
            })
            .catch(reject => {
                return [];
            });
    }

    //add new product to database 
    async addProduct() {
        this.newProduct.dateAdded = new Date().getTime().toString();
        this.newProduct.dateUpdated = new Date().getTime().toString();

        //check if input fields are empty
        if (
            this.newProduct.productName == '' ||
            this.newProduct.locationId == '' ||
            this.newProduct.price == '' ||
            this.newProduct.count == '' ||
            this.newProduct.countMax == ''
        ) {
            this.showToast('top', 3000, 'No empty fields please!');
            return;
        }

        //compare count with countmax
        if (+this.newProduct.count > +this.newProduct.countMax) {
            this.showToast(
                'top',
                3000,
                'The count is greater than the maximum count!'
            );
            return;
        }

        let nextStep = await this.productService
            .insertProduct(this.newProduct)
            .then(resolve => {
                this.showToast('top', 3000, 'Inserted the new product!');
                // resets the product
                this.newProduct = {
                    productName: '',
                    locationId: '',
                    price: '',
                    count: '',
                    countMax: '',
                    dateAdded: new Date().getTime().toString(),
                    dateUpdated: new Date().getTime().toString(),
                    deleted: '0'
                };
                return true;
            })
            .catch(reject => {
                this.showToast(
                    'top',
                    3000,
                    'Could not insert the new product!'
                );
                return false;
            });

        //check for success of previous step, otherwise return
        if (!nextStep) {
            return;
        }

        //read locations and convert to tArray
        this.locationService.getLocations().then((locations: Location[]) => {
            let tArray: any[] = [];
            locations.forEach((el: Location) => {
                // inits the data for the dropdown
                tArray.push({
                    _id: el._id,
                    locationName: el.locationName,
                    drinks: this.assignDrinks(el._id)
                });
            });

            // inits the dropdown data
            this.locationDropdown = tArray;

            // inits the presenting data of the page
            this.selectedLocation = Object.assign({}, tArray[0]);
        });
    }

    /**
     * Sets the data to the selectedLocation
     * @param item
     */
    selectDropdownLocation(item: any): void {
        this.selectedLocation = Object.assign({}, item);
    }

    /**
     * Assigns the drinks to the location
     * @param locationId
     */
    assignDrinks(locationId: string): Promise<Product[]> {
        return new Promise((resolve, reject) => {
            this.productService.getProducts().then((products: Product[]) => {
                let filteredProducts: Product[] = products
                    .filter(el => el.locationId === locationId)
                    .map(el => {
                        // adds the buyDrinkCount attribute to the productsObject
                        return Object.assign({ buyDrinkCount: 0 }, el);
                    });
                if (filteredProducts.length == 0 || !products) {
                    reject([]);
                }
                resolve(filteredProducts);
            });
        });
    }

    //edit product information
    async setProduct(drink: Product) {
        this.editedProduct._id = drink._id;
        this.editedProduct.productName = drink.productName;
        this.editedProduct.locationId = drink.locationId;
        this.editedProduct.price = drink.price;
        this.editedProduct.count = +drink.count + drink.buyDrinkCount;
        this.editedProduct.countMax = drink.countMax;
        this.editedProduct.dateAdded = drink.dateAdded;
        this.editedProduct.dateUpdated = new Date().getTime().toString();
        this.editedProduct.deleted = drink.deleted;

        await this.productService
            .updateProduct(this.editedProduct)
            .then(resolve => {
                this.showToast(
                    'top',
                    3000,
                    `Stocked up ${drink.productName} with ${
                        drink.buyDrinkCount
                    } items.`
                );
                return true;
            })
            .catch(reject => {
                this.showToast('top', 3000, 'Could not updata data!');
                return false;
            });
    }

    async deleteProduct(drink: Product) {
        drink.deleted = '1';
        delete drink.buyDrinkCount;

        await this.productService
            .updateProduct(drink)
            .then(resolve => {
                this.showToast('top', 3000, `Deleted ${drink.productName}`);
                return true;
            })
            .catch(reject => {
                this.showToast('top', 3000, 'Could not delete!');
                return false;
            });

        await this.readLocations();

        //read locations and convert to tArray
        this.locationService.getLocations().then((locations: Location[]) => {
            let tArray: any[] = [];
            locations.forEach((el: Location) => {
                // inits the data for the dropdown
                tArray.push({
                    _id: el._id,
                    locationName: el.locationName,
                    drinks: this.assignDrinks(el._id)
                });
            });

            // inits the dropdown data
            this.locationDropdown = tArray;

            // inits the presenting data of the page
            this.selectedLocation = Object.assign({}, tArray[0]);
        });
    }

    /**
     * Shows a toast
     * @param position
     * @param message
     */
    showToast(position: string, duration: number, message: string): void {
        let toast = this.toastCtrl.create({
            message: message,
            duration: duration,
            position: position
        });

        toast.present(toast);
    }
}
