import {
    TestBed,
    ComponentFixture,
    async,
    inject
} from '@angular/core/testing';
import { IonicModule } from 'ionic-angular';
import { InventoryPage } from './inventory';

let comp: InventoryPage;
let fixture: ComponentFixture<InventoryPage>;

describe('Component: Root Component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [InventoryPage],

            providers: [],

            imports: [IonicModule.forRoot(InventoryPage)]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(InventoryPage);
        comp = fixture.componentInstance;
    });

    afterEach(() => {
        fixture.destroy();
        comp = null;
    });

    describe('#addProduct', () => {
        // TODO: create test
    });
    describe('#setProduct', () => {
        // TODO: create test
    });
    describe('#deleteProduct', () => {
        // TODO: create test
    });
});
