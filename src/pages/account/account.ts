import { Component } from '@angular/core';
import {
    NavController,
    LoadingController,
    ToastController,
    AlertController
} from 'ionic-angular';
import { App } from 'ionic-angular';
import { UserService } from '../../services/userService';
import { AuthService } from '../../services/authService';
import { CookieService } from 'ngx-cookie-service';
import { LoginPage } from '../login/login';
import { LocationService } from '../../services/locationService';
import { User } from '../../aids/user';

@Component({
    selector: 'page-account',
    templateUrl: 'account.html'
})
export class AccountPage {
    constructor(
        public navCtrl: NavController,
        public loadingCtrl: LoadingController,
        public toastCtrl: ToastController,
        public userService: UserService,
        public authService: AuthService,
        private cookieService: CookieService,
        public locationService: LocationService,
        public appCtrl: App,
        public alerCtrl: AlertController
    ) {}

    cookieValue = 'UNKNOWN';

    locations: any[];

    oldUser: User = {
        _id: '',
        email: '',
        password: ''
    };

    newUser: User = {
        _id: '',
        email: '',
        password: ''
    };

    userData: User = {
        _id: '',
        email: '',
        locationid: '',
        password: '',
        roomid: '',
        monthlyLimit: 0,
        bankingData: ''
    };

    newPassword: { newPassword: string; newPasswordRepeat: string } = {
        newPassword: '',
        newPasswordRepeat: ''
    };

    async ngOnInit(): Promise<void> {
        //this.cookieService.set( 'Test', 'Hello World' );
        this.cookieValue = this.cookieService.get('token');

        //read location data for location drop down menue
        await this.readLocations();

        //get User data with token, write data to newUser
        await this.userService
            .getUserFromToken()
            .then(user => {
                this.setUserData(user);

                return true;
            })
            .catch(() => {
                this.showToast('top', 3000, 'Wrong Token Data!');

                return false;
            });
    }

    //read Location's _ids and names for dropdown menue
    async readLocations() {
        this.locations = await this.locationService
            .getLocations()
            .then(location => {
                return location;
            })
            .catch(() => {
                return [];
            });
    }

    
    async accountSetting() {
        //check if password fields empty
        if (
            this.oldUser.password == '' ||
            this.newPassword.newPassword == '' ||
            this.newPassword.newPasswordRepeat == ''
        ) {
            this.showToast(
                'top',
                3000,
                'One or more password field(s) is empty!'
            );

            return;
        } 
        //compare new password fields for identity
        else if (
            this.newPassword.newPassword != this.newPassword.newPasswordRepeat
        ) {
            this.showToast('top', 3000, 'Password fields are not identical!');

            return;
        } 
        //compare old and new password for identity
        else if (this.oldUser.password == this.newPassword.newPassword) {
            let toast = this.toastCtrl.create({
                message: 'Old and new password identical!',
                duration: 1200,
                position: 'top'
            });
            toast.present();
            return;
        }

        //verify oldUser password with login try
        var nextStep = await this.authService
            .auth(this.oldUser)
            .then(() => {
                return true;
            })
            .catch(() => {
                this.showToast('top', 3000, 'Wrong old password!');

                return false;
            });

        //check for success of previous step, otherwise return
        if (!nextStep) {
            return;
        }

        //create newUser to be sent to database
        this.newUser._id = this.oldUser._id;
        this.newUser.email = this.oldUser.email;
        this.newUser.password = this.newPassword.newPassword;

        //update User in mongoDB with newUser
        nextStep = await this.userService
            .updateUser(this.newUser)
            .then(() => {
                return true;
            })
            .catch(() => {
                this.showToast('top', 3000, 'Could not update Data!');

                return false;
            });

        //check for success of previous step, otherwise return
        if (!nextStep) {
            return;
        }

        this.showToast('top', 3000, 'Updated password data!');
    }

    async updateData() {

        //update User in mongoDB with newUser
        await this.userService
            .updateUser(this.userData)
            .then(() => {
                return true;
            })
            .catch(() => {
                this.showToast('top', 3000, 'Could not update Data!');

                return false;
            });

        //get User data with token, write data to newUser
        await this.userService
            .getUserFromToken()
            .then(user => {
                this.setUserData(user);

                return true;
            })
            .catch(() => {
                this.showToast('top', 3000, 'Wrong Token Data!');

                return false;
            });

        this.showToast('top', 3000, 'Updated user data!');
    }

    async logout() {
        //Show logout confirmation and if success setRoot page to LoginPage and logout in authService
        let confirm = this.alerCtrl.create({
            title: `Logout Bestätigeung`,
            message: `Du wirst ausloggen, klicke auf <span class="logoutMessage">BESTÄTIGEN </span> um fortzufahren`,
            cssClass: 'alertCustomCss',
            buttons: [
                {
                    text: 'Abbrechen',
                    handler: () => {}
                },
                {
                    text: 'Bestätigen',
                    handler: async () => {
                        this.appCtrl.getRootNav().setRoot(LoginPage);

                        this.authService.logout();
                        this.showToast('top', 3000, 'Logout successful');
                    }
                }
            ]
        });
        confirm.present();
    }

    //set User Data of old and new user 
    setUserData(user: User): void {
        this.oldUser._id = user['_id'];
        this.oldUser.email = user['email'];
        this.userData._id = user['_id'];
        this.userData.email = user['email'];
        this.userData.locationid = user['locationid'];
        this.userData.roomid = user['roomid'];
        this.userData.monthlyLimit = user['monthlyLimit'];
        this.userData.bankingData = user['bankingData'];
    }
    //top messages function
    showToast(position: string, duration: number, message: string) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: duration,
            position: position
        });

        toast.present(toast);
    }
}
