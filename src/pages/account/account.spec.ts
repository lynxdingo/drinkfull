import { async, TestBed } from '@angular/core/testing';
import {
    IonicModule,
    NavController,
    LoadingController,
    ToastController,
    App,
    AlertController,
    Config,
    Platform
} from 'ionic-angular';

import { AccountPage } from './account';

import {
    LocationServiceMock,
    UserServiceMock,
    AuthServiceMock,
    CookieServiceMock,
    NavControllerMock,
    LoadingControllerMock,
    ToastControllerMock,
    AppMock,
    AlertControllerMock,
    ConfigMock,
    PlatformMock,
    HttpClientMock,
    HttpHandlerMock
} from '../../../test-config/mocks-ionic';

import { LocationService } from '../../services/locationService';
import { AuthService } from '../../services/authService';
import { UserService } from '../../services/userService';
import { CookieService } from 'ngx-cookie-service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('Account Page', () => {
    let fixture;
    let component;

    let usreSvc,
        authSvc,
        navCtrl,
        loadingCtrl,
        toastCtrl,
        appCtrl,
        cookieSvc,
        alertCtrl;
    let ctrl;

    // Spy Ons
    let locationServiceSpy: jasmine.SpyObj<LocationService>;

    // class variables
    let cookieValue;
    let locations: any[];
    let oldUser, newUser, userData;
    let newPassword: any = {};

    beforeEach(async(() => {
        const spyLocationService = jasmine.createSpyObj('LocationService', [
            'getLocations',
            'getLocations.then'
        ]);

        TestBed.configureTestingModule({
            declarations: [AccountPage],
            providers: [
                { provide: LocationService, useValue: spyLocationService },
                { provide: UserService, useClass: UserServiceMock },
                { provide: AuthService, useClass: AuthServiceMock },
                { provide: NavController, useClass: NavControllerMock },
                { provide: LoadingController, useClass: LoadingControllerMock },
                { provide: ToastController, useClass: ToastControllerMock },
                { provide: Platform, useClass: PlatformMock },
                { provide: HttpClient, useClass: HttpClientMock },
                { provide: HttpHandler, useClass: HttpHandlerMock },
                { provide: App, useClass: AppMock },
                { provide: Config, useClass: ConfigMock },
                { provide: AlertController, useClass: AlertControllerMock },
                { provide: CookieService, useClass: CookieServiceMock }
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        });
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AccountPage);
        component = fixture.componentInstance;

        // Providers
        locationServiceSpy = TestBed.get(LocationService);
        usreSvc = TestBed.get(UserService);
        authSvc = TestBed.get(AuthService);
        navCtrl = TestBed.get(NavController);
        loadingCtrl = TestBed.get(LoadingController);
        toastCtrl = TestBed.get(ToastController);
        appCtrl = TestBed.get(App);
        cookieSvc = TestBed.get(CookieService);
        alertCtrl = TestBed.get(AlertController);

        // Controller
        ctrl = new AccountPage(
            navCtrl,
            loadingCtrl,
            toastCtrl,
            usreSvc,
            authSvc,
            cookieSvc,
            locationServiceSpy,
            appCtrl,
            alertCtrl
        );
    });

    it('should be created', () => {
        expect(component instanceof AccountPage).toBeDefined();
    });

    xdescribe('#readLocations', () => {
        it('should return the locations', async done => {
            const stubValue = 'locations';
            // getLocationsSpy = locationServiceSpy.getLocations.and.returnValue(of(stubValue));

            try {
                let called = await ctrl.readLocations();

                expect(called).toBe(stubValue, 'service returned stub value');
                expect(locationServiceSpy.getLocations.calls.count()).toBe(
                    1,
                    'spy method called once'
                );
                expect(
                    locationServiceSpy.getLocations.calls.mostRecent()
                        .returnValue
                ).toBe(stubValue);
                done();
            } catch (err) {
                console.log(err);
                done.fail();
            }
        });
    });
    describe('#accountSetting', () => {
        // TODO: write test
    });
    describe('#setUserData', () => {
        // TODO: write test
    });
});
