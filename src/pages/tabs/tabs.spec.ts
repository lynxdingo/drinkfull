import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { TabsPage } from './tabs';
import { IonicModule } from 'ionic-angular/index';

import { AlertController, NavController, NavParams } from 'ionic-angular';
import { NavParamsMock  } from '../../../test-config/mocks-ionic';




describe('TabPage', () => {
  let de: DebugElement;
  let comp: TabsPage;
  let fixture: ComponentFixture<TabsPage>;

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       declarations: [TabsPage],
//       imports: [
//         IonicModule.forRoot(TabsPage)
//       ],
//       providers: [
//         NavController,
//         NavParams, 
//         AlertController,
//         { provide: NavParams, useClass: NavParamsMock },

//       ]
//     });
//   }));

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TabsPage],
      providers: [
        NavController,
        NavParams, 
        AlertController,
        { provide: NavParams, useClass: NavParamsMock },

      ],
      imports: [
        IonicModule.forRoot(TabsPage)
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabsPage);
    comp = fixture.debugElement.componentInstance;
    fixture.detectChanges();
    // NavParamsMock.setParams(true); //set your own params here
  });

//   it('should create component', () => expect(comp).toBeDefined());
// it('Should have one device if is received by navparams', () => {
//     comp.asAdmin = {
//       _id: "001", 
//      name: "Mike", 
//     }

//     chai.expect(component.user._id).to.be.equal("001");
//   });



});
