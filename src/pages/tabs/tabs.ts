import { Component } from '@angular/core';

import { AlertController, NavController, NavParams } from 'ionic-angular';

import { LobbyPage } from '../lobby/lobby';
import { HistoryPage } from '../history/history';
import { AccountPage } from '../account/account';
import { InventoryPage } from '../inventory/inventory';
import { BankingPage } from '../banking/banking';
import { UsersPage } from '../users/users';
import { AdminAccountPage } from '../adminAccount/adminAccount';
import { historySegment } from '../history/segments/historySegment';
import { invoiceSegmentPage } from '../history/segments/invoiceSegment';

@Component({
    templateUrl: 'tabs.html'
})
export class TabsPage {
    tab1Root = LobbyPage;
    tab3Root = HistoryPage;
    tab6Root = AccountPage;
    tab7Root = InventoryPage;
    tab8Root = BankingPage;
    tab9Root = UsersPage;
    tab10Root = AdminAccountPage;
    seg1Root = historySegment;
    seg2Root = invoiceSegmentPage;

    public asAdmin: boolean;

    constructor(
        public alertCtrl: AlertController,
        public navCtrl: NavController,
        public navParams: NavParams
    ) {
        this.asAdmin = navParams.get('asAdmin');
    }
}
