import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpClientModule } from '@angular/common/http';

//Validation with npm package ng2-validation
import { FormsModule } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';

// Pages
import { LoginPage } from '../pages/login/login';
import { TabsPage } from '../pages/tabs/tabs';
import { PopoverPage } from '../components/popover';

// Pages - User
import { HistoryPage } from '../pages/history/history';
import { LobbyPage } from '../pages/lobby/lobby';
import { AccountPage } from '../pages/account/account';

// Pages - Admin
import { InventoryPage } from '../pages/inventory/inventory';
import { BankingPage } from '../pages/banking/banking';
import { UsersPage } from '../pages/users/users';
import { AdminAccountPage } from '../pages/adminAccount/adminAccount';

// Components
import { DrinkButtonComponent } from '../components/drinkButton';
import { DrinkSelectorComponent } from '../components/drinkSelector';
import { historySegment } from '../pages/history/segments/historySegment';
import { invoiceSegmentPage } from '../pages/history/segments/invoiceSegment';
import { productTable } from '../components/productTable';

// Services
import { AuthService } from '../services/authService';
import { LocationService } from '../services/locationService';
import { OrderService } from '../services/orderService';
import { ProductService } from '../services/productService';
import { UserService } from '../services/userService';

//Pipes
import { PipesModule } from '../pipes/pipes.module';
import { OrderLocationPipe } from '../pipes/order-location/order-location';
import { OrderDatePipe } from '../pipes/order-date/order-date';
import { IntegrateOrdersPipe } from '../pipes/integrate-orders/integrate-orders';

import { CookieService } from 'ngx-cookie-service';

import { DatePipe } from '@angular/common';
@NgModule({
    declarations: [
        MyApp,
        AccountPage,
        HistoryPage,
        LobbyPage,
        TabsPage,
        InventoryPage,
        BankingPage,
        UsersPage,
        AdminAccountPage,
        DrinkButtonComponent,
        DrinkSelectorComponent,
        PopoverPage,
        LoginPage,
        historySegment,
        invoiceSegmentPage,
        productTable
    ],
    imports: [
        BrowserModule,
        FormsModule,
        CustomFormsModule, //Validation
        IonicModule.forRoot(MyApp, { tabsPlacement: 'top' }),
        HttpClientModule,
        PipesModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        AccountPage,
        historySegment,
        invoiceSegmentPage,
        LobbyPage,
        TabsPage,
        InventoryPage,
        BankingPage,
        UsersPage,
        AdminAccountPage,
        PopoverPage,
        LoginPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        { provide: ErrorHandler, useClass: IonicErrorHandler },
        AuthService,
        LocationService,
        OrderService,
        ProductService,
        UserService,
        CookieService,
        DatePipe,
        OrderLocationPipe,
        OrderDatePipe,
        IntegrateOrdersPipe
    ]
})
export class AppModule {}
