import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { User } from '../aids/user';
@Injectable()
export class UserService {
    constructor(
        private http: HttpClient,
        private cookieService: CookieService
    ) {}

    cookieValue = 'UNKNOWN';

    ngOnInit(): void {
        this.cookieValue = this.cookieService.get('token');
    }

    getUsers(): Promise<User[]> {
        return new Promise((resolve, reject) => {
            this.cookieValue = this.cookieService.get('token');
            this.http
                .get('https://pwp.um.ifi.lmu.de/g08/users/'+this.cookieValue)
                .subscribe((users: User[]) => {
                    if (!users) {
                        reject([]);
                    }
                    resolve(users);
                });
        });
    }

    insertUser(user: User): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.cookieValue = this.cookieService.get('token');
            this.http
                .post('https://pwp.um.ifi.lmu.de/g08/users/insert', {
                    token: this.cookieValue,
                    content: user
                })
                .subscribe(data => {
                    if (data['success'] == 'true') {
                        resolve(true);
                    } else {
                        reject(false);
                    }
                });
        });
    }

    updateUser(user: User): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.cookieValue = this.cookieService.get('token');
            this.http
                .post('https://pwp.um.ifi.lmu.de/g08/users/update', {
                    token: this.cookieValue,
                    content: user
                })
                .subscribe(data => {
                    if (data['success'] == 'true') {
                        resolve(true);
                    } else {
                        reject(false);
                    }
                });
        });
    }

    getUserFromToken(): Promise<User> {
        return new Promise((resolve, reject) => {
            this.cookieValue = this.cookieService.get('token');

            this.http
                .get('https://pwp.um.ifi.lmu.de/g08/me/' + this.cookieValue)
                .subscribe(data => {
                    if (data[0]['_id']) {
                        resolve(data[0]);
                    } else {
                        reject(false);
                    }
                });
        });
    }

    /**
     * Example
     * @param idJson {"_id": "5c3b82cfb44446d11fe5d0f2"}
     */
    // TODO: adjust function
    movedoutUser(idJson: any[]): any {
        return new Promise((resolve, reject) => {
            this.http
                .post(`https://pwp.um.ifi.lmu.de/g08/users/movedout`, {
                    idJson
                })
                .subscribe(ret => console.log({ ret }));
        });
    }
}
