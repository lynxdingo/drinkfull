import { TestBed } from '@angular/core/testing';

import { AuthService } from './authService';
import { LocationService } from './locationService';
import { UserService } from './userService';
import { ProductService } from './productService';
import { OrderService } from './orderService';
import { Location } from '../aids/location';
import { User } from '../aids/user';
import { Product } from '../aids/product';
import { Order } from '../aids/order';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {
    HttpClientTestingModule,
    HttpTestingController
} from '@angular/common/http/testing';
import { CookieService } from 'ngx-cookie-service';

describe('Services', () => {
    let httpClient: HttpClient;
    let httpTestingController: HttpTestingController;

    let httpClientSpy: jasmine.SpyObj<HttpClient>;
    let cookieServiceSpy: jasmine.SpyObj<CookieService>;
    let userServiceSpy: jasmine.SpyObj<UserService>;

    describe('AuthService', () => {
        let service: AuthService;
        let user: User = {
            _id: '1',
            email: 'test@example.com',
            password: 'password',
            isAdmin: '0'
        };

        beforeEach(() => {
            const spyCookieService = jasmine.createSpyObj('CookieService', [
                'deleteAll'
            ]);

            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule],
                providers: [
                    AuthService,
                    { provide: CookieService, useValue: spyCookieService }
                ]
            });

            // Inject both the service-to-test and its (spy) dependency
            service = TestBed.get(AuthService);
            cookieServiceSpy = TestBed.get(CookieService);

            // Inject the http service and test controller for each test
            httpClient = TestBed.get(HttpClient);
            httpTestingController = TestBed.get(HttpTestingController);
        });

        it('#auth should check if user authenticates', () => {
            httpClient
                .post<any>('https://pwp.um.ifi.lmu.de/g08/login', user)
                .subscribe(data => {
                    let auth = data['auth'];

                    expect(typeof auth).toEqual(
                        'boolean',
                        'resolves a boolean'
                    );
                });
        });

        it('#logout should delete cookies', () => {
            let retValue = true;
            cookieServiceSpy.deleteAll.and.returnValue(retValue);

            service.logout();
            expect(cookieServiceSpy.deleteAll).toHaveBeenCalledTimes(1);
            expect(
                cookieServiceSpy.deleteAll.calls.mostRecent().returnValue
            ).toBe(retValue, 'deleteAll been succesful');
        });
    });

    describe('LocationService', () => {
        let service: LocationService;
        let locations: Location[] = [{ _id: '', locationName: 'string' }];

        beforeEach(() => {
            const spyCookieService = jasmine.createSpyObj('CookieService', [
                'deleteAll'
            ]);

            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule],
                providers: [
                    LocationService,
                    { provide: CookieService, useValue: spyCookieService }
                ]
            });

            // Inject both the service-to-test and its (spy) dependency
            service = TestBed.get(LocationService);
            cookieServiceSpy = TestBed.get(CookieService);

            // Inject the http service and test controller for each test
            httpClient = TestBed.get(HttpClient);
            httpTestingController = TestBed.get(HttpTestingController);
        });

        it('#getLocations', () => {
            httpClient
                .get<any>('https://pwp.um.ifi.lmu.de/g08/locations/')
                .subscribe((locations: Location[]) => {
                    expect(locations).toEqual(this.locations);
                });
        });
    });

    describe('UserService', () => {
        let service: UserService;
        let user: User = {
            _id: '1',
            email: 'test@example.com',
            password: 'password',
            isAdmin: '0'
        };
        let cookieValue = 'string';

        beforeEach(() => {
            const spyCookieService = jasmine.createSpyObj('CookieService', [
                'deleteAll'
            ]);

            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule],
                providers: [
                    UserService,
                    { provide: CookieService, useValue: spyCookieService }
                ]
            });

            // Inject both the service-to-test and its (spy) dependency
            service = TestBed.get(UserService);
            cookieServiceSpy = TestBed.get(CookieService);

            // Inject the http service and test controller for each test
            httpClient = TestBed.get(HttpClient);
            httpTestingController = TestBed.get(HttpTestingController);
        });

        it('#getUsers', () => {
            httpClient
                .get<any>('https://pwp.um.ifi.lmu.de/g08/users/')
                .subscribe((users: User[]) => {
                    expect(users).toEqual(this.users);
                });
        });
        it('#insertUser', () => {
            httpClient
                .post<any>('https://pwp.um.ifi.lmu.de/g08/users/insert', {
                    cookieValue,
                    user
                })
                .subscribe(data => {
                    let iuser = data['success'];

                    expect(typeof iuser).toEqual(
                        'boolean',
                        'resolves a boolean'
                    );
                });
        });
        it('#updateUser', () => {
            httpClient
                .post<any>('https://pwp.um.ifi.lmu.de/g08/users/update', {
                    cookieValue,
                    user
                })
                .subscribe(data => {
                    let iuser = data['success'];

                    expect(typeof iuser).toEqual(
                        'boolean',
                        'resolves a boolean'
                    );
                });
        });
        it('#getUserFromToken', () => {
            httpClient
                .get<any>('https://pwp.um.ifi.lmu.de/g08/me/')
                .subscribe((locations: Location[]) => {
                    expect(typeof user[0]['_id']).toEqual(
                        'boolean',
                        'resolves a boolean'
                    );
                });
        });
    });

    describe('ProductService', () => {
        let service: ProductService;
        let user: User = {
            _id: '1',
            email: 'test@example.com',
            password: 'password',
            isAdmin: '0'
        };
        let products: Product[] = [
            {
                productName: '',
                locationId: '',
                price: '',
                count: '',
                countMax: '',
                dateAdded: '',
                dateUpdated: '',
                deleted: ''
            }
        ];
        let cookieValue = 'string';
        let idJson = { _id: '5c3b82cfb44446d11fe5d0f2' };

        beforeEach(() => {
            const spyCookieService = jasmine.createSpyObj('CookieService', [
                'deleteAll'
            ]);

            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule],
                providers: [
                    ProductService,
                    { provide: CookieService, useValue: spyCookieService }
                ]
            });

            // Inject both the service-to-test and its (spy) dependency
            service = TestBed.get(ProductService);
            cookieServiceSpy = TestBed.get(CookieService);

            // Inject the http service and test controller for each test
            httpClient = TestBed.get(HttpClient);
            httpTestingController = TestBed.get(HttpTestingController);
        });

        it('#getProducts', () => {
            httpClient
                .get<any>('https://pwp.um.ifi.lmu.de/g08/products/')
                .subscribe((products: Product[]) => {
                    expect(products).toEqual(this.products);
                });
        });
        it('#insertProduct', () => {
            httpClient
                .post<any>('https://pwp.um.ifi.lmu.de/g08/products/insert', {
                    cookieValue,
                    products
                })
                .subscribe(data => {
                    let iprod = data['success'];

                    expect(typeof iprod).toEqual(
                        'boolean',
                        'resolves a boolean'
                    );
                });
        });
        it('#updateUser', () => {
            httpClient
                .post<any>('https://pwp.um.ifi.lmu.de/g08/products/update', {
                    cookieValue,
                    products
                })
                .subscribe(data => {
                    let iprod = data['success'];

                    expect(typeof iprod).toEqual(
                        'boolean',
                        'resolves a boolean'
                    );
                });
        });
        it('#hideProduct', () => {
            httpClient
                .post<any>('https://pwp.um.ifi.lmu.de/g08/products/hide', {
                    idJson
                })
                .subscribe(data => {
                    let iprod = data['success'];
                    expect(typeof iprod).toEqual(iprod);
                });
        });
    });

    describe('OrderService', () => {
        let service: OrderService;
        let user: User = {
            _id: '1',
            email: 'test@example.com',
            password: 'password',
            isAdmin: '0'
        };
        let orders: Order[] = [
            {
                productId: '',
                locationId: '',
                userId: '',
                orderDate: '',
                price: '',
                deleted: ''
            }
        ];
        let product: Product = {
            _id: '1',
            productName: '',
            locationId: '',
            price: '',
            count: '',
            countMax: '',
            dateAdded: '',
            dateUpdated: '',
            deleted: ''
        };
        let cookieValue = 'string';
        let idJson = { _id: '5c3b82cfb44446d11fe5d0f2' };

        beforeEach(() => {
            const spyUserService = jasmine.createSpyObj('UserService', [
                'getUserFromToken'
            ]);

            const spyCookieService = jasmine.createSpyObj('UserService', [
                'get'
            ]);

            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule],
                providers: [
                    OrderService,
                    { provide: CookieService, useValue: spyCookieService },
                    { provide: UserService, useValue: spyUserService }
                ]
            });

            // Inject both the service-to-test and its (spy) dependency
            service = TestBed.get(OrderService);
            cookieServiceSpy = TestBed.get(CookieService);
            userServiceSpy = TestBed.get(UserService);

            // Inject the http service and test controller for each test
            httpClient = TestBed.get(HttpClient);
            httpTestingController = TestBed.get(HttpTestingController);
        });

        it('#getOrders', () => {
            httpClient
                .get<any>('https://pwp.um.ifi.lmu.de/g08/orders/')
                .subscribe((orders: Order[]) => {
                    expect(orders).toEqual(this.orders);
                });
        });
        it('#insertOrder', () => {
            httpClient
                .post<any>('https://pwp.um.ifi.lmu.de/g08/orders/insert', {
                    cookieValue,
                    orders
                })
                .subscribe(data => {
                    let iprod = data['success'];
                    expect(typeof iprod).toEqual(
                        'boolean',
                        'resolves a boolean'
                    );
                });
        });
        it('#deleteOrder', () => {
            httpClient
                .post<any>('https://pwp.um.ifi.lmu.de/g08/orders/delete', {
                    idJson
                })
                .subscribe(data => {
                    let iprod = data['success'];
                    expect(typeof iprod).toEqual(iprod);
                });
        });
        xit('#createOrder', async () => {
            let retValue = user;
            userServiceSpy.getUserFromToken.and.returnValue(retValue);
            let called = await service.createOrder(product, 1, '1');
            expect(typeof called).toEqual('boolean');
        });
    });
});
