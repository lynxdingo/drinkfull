import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { CookieService } from 'ngx-cookie-service';
@Injectable()
export class AuthService {
    constructor(
        private http: HttpClient,
        private cookieService: CookieService
    ) {}

    auth(user: any): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.http
                .post('https://pwp.um.ifi.lmu.de/g08/login', {
                    email: user.email,
                    password: user.password,
                    asAdmin: user.asAdmin,
                    asAdminLastLogin: user.asAdminLastLogin
                })
                .subscribe(data => {
                    if (data['auth'] == true) {
                        this.cookieService.set('token', data['token']);
                        resolve(true);
                    } else {
                        reject(false);
                    }
                });
        });
    }

    logout() {
        this.cookieService.deleteAll();
    }
}
