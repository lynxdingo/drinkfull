import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from '../aids/product';
import { Order } from '../aids/order';

import { CookieService } from 'ngx-cookie-service';
@Injectable()
export class ProductService {

    constructor(
        private http: HttpClient,
        private cookieService: CookieService
    ) {}

    cookieValue = 'UNKNOWN';

    ngOnInit(): void {
        this.cookieValue = this.cookieService.get('token');
    }

    // --- ROUTE --- //
    getProducts(): Promise<Product[]> {
        return new Promise((resolve, reject) => {
            this.cookieValue = this.cookieService.get('token');
            this.http
                .get('https://pwp.um.ifi.lmu.de/g08/products/'+this.cookieValue)
                .subscribe((products: Product[]) => {
                    resolve(products);
                });
        });
    }

    insertProduct(product: Product): Promise<boolean> {
        delete product.buyDrinkCount;
        return new Promise((resolve, reject) => {
            this.cookieValue = this.cookieService.get('token');
            this.http
                .post(`https://pwp.um.ifi.lmu.de/g08/products/insert`, {
                    token: this.cookieValue,
                    content: product
                })
                .subscribe(ret => {
                    resolve(ret['success']);
                });
        });
    }

    updateProduct(product: Product): Promise<boolean> {
        delete product.buyDrinkCount;
        return new Promise((resolve, reject) => {
            this.cookieValue = this.cookieService.get('token');
            this.http
                .post(`https://pwp.um.ifi.lmu.de/g08/products/update`, {
                    token: this.cookieValue,
                    content: product
                })
                .subscribe(ret => {
                    resolve(ret['success']);
                });
        });
    }

    /**
     * Example
     * @param idJson {"_id": "5c3b82cfb44446d11fe5d0f2"}
     */
    // TODO: adjust function
    hideProduct(idJson: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.http
                .post(`https://pwp.um.ifi.lmu.de/g08/products/hide`, { idJson })
                .subscribe(ret => {
                    resolve(ret['success']);
                });
        });
    }

    // --- LOCAL --- //

    /**
     * Formats the date to a German standard
     * @param date
     */
    formatDate(date: Date): string {
        let dayNames = [
            'Sonntag',
            'Montag',
            'Dienstag',
            'Mittwoch',
            'Donnerstag',
            'Freitag',
            'Samstag'
        ];

        let dayIndex = date.getDay();
        let day = date.getDate();
        let month = date.getMonth() + 1;
        let year = date.getFullYear();

        return dayNames[dayIndex] + ', ' + day + '.' + month + '.' + year;
    }

    /**
     * Gets the information of the specific product
     * @param orderObj the product that needs additional information
     * @param productsList all the products
     */
    getProductInformation(orderObj: Order, productsList: Product[]): Product {
        // the products filtered by the location
        let filteredLocation: Product[] = productsList.filter(
            el => el.locationId === orderObj.locationId
        );
        // the product in the productsList
        let tProduct: Product = filteredLocation.find(
            el => el._id === orderObj.productId
        );

        if (tProduct) {
            let date: Date = new Date(+orderObj.orderDate);

            // adding additional data to the product
            tProduct = Object.assign(
                {
                    buyDrinkCount: 1,
                    date: date.toDateString(),
                    dateFormated: this.formatDate(date),
                    time: date.toTimeString().match(/\d+:\d+:\d+/)[0]
                },
                tProduct
            );
        }

        return tProduct;
    }
}
