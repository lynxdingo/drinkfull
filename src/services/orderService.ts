import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserService } from './userService';
import { Order } from '../aids/order';
import { Product } from '../aids/product';

import { CookieService } from 'ngx-cookie-service';
@Injectable()
export class OrderService {
    constructor(
        private http: HttpClient,
        public userSvc: UserService,
        private cookieService: CookieService
    ) {}

    cookieValue = 'UNKNOWN';

    ngOnInit(): void {
        this.cookieValue = this.cookieService.get('token');
    }

    // --- ROUTES --- //
    getOrders(): Promise<Order[]> {
        return new Promise((resolve, reject) => {
            this.cookieValue = this.cookieService.get('token');
            this.http
                .get(`https://pwp.um.ifi.lmu.de/g08/orders/`+this.cookieValue)
                .subscribe((orders: Order[]) => {
                    if (!orders) {
                        reject([]);
                    }
                    resolve(orders);
                });
        });
    }

    insertOrder(orders: Order[]): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.cookieValue = this.cookieService.get('token');
            this.http
                .post(`https://pwp.um.ifi.lmu.de/g08/orders/insert`, {
                    token: this.cookieValue,
                    content: orders
                })
                .subscribe(ret => {
                    resolve(ret['success']);
                });
        });
    }

    /**
     * Example
     * @param idJson {"_id": "5c3b82cfb44446d11fe5d0f2"}
     */
    deleteOrder(idJson: any): any {
        return new Promise((resolve, reject) => {
            this.http
                .post(`https://pwp.um.ifi.lmu.de/g08/orders/delete`, {
                    orderId: idJson
                })
                .subscribe(ret => {
                    resolve(ret['success']);
                });
        });
    }

    // --- LOCAL --- //
    /**
     * Creates an order
     * @param product the product that you want to purchase
     * @param quantity the quantity of the product
     * @param locationId the location it was purchased
     */
    async createOrder(
        product: Product,
        quantity: number,
        locationId: string
    ): Promise<boolean> {
        const userData: any = await this.userSvc.getUserFromToken();

        let order: Order = {
            productId: product._id,
            locationId: locationId,
            userId: userData._id,
            orderDate: new Date().getTime(),
            price: product.price,
            deleted: false
        };
        // copies of the order
        let orders: Order[] = new Array(quantity).fill(order);

        const createdOrder = await this.insertOrder(orders);
        return createdOrder;
    }
}
