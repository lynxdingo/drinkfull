import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Location } from '../aids/location';

import { CookieService } from 'ngx-cookie-service';
@Injectable()
export class LocationService {
    constructor(
        private http: HttpClient,
        private cookieService: CookieService
    ) {}
    
    cookieValue = 'UNKNOWN';

    ngOnInit(): void {
        this.cookieValue = this.cookieService.get('token');
    }

    getLocations(): Promise<Location[]> {
        return new Promise((resolve, reject) => {
            this.cookieValue = this.cookieService.get('token');
            this.http
                .get('https://pwp.um.ifi.lmu.de/g08/locations/'+this.cookieValue)
                .subscribe((locations: Location[]) => {
                    resolve(locations);
                });
        });
    }
}
