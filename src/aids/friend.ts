//define the different drink types

export class SingleFriend {
    name: string;
    //Problem: define a default value like this is not correct:
    // liked?: boolean = true;
    liked: boolean;
    drink?: string;
    accepted?: boolean;
    exists?: boolean;
}
