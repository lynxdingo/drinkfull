export interface Product {
    // Defined by database
    _id?: string;
    productName: string;
    locationId: string;
    price: string;
    count: number|string;
    countMax: number|string;
    dateAdded: number|string;
    dateUpdated: number|string;
    deleted: boolean|string;

    // Local attributes
    time?: string;
    date?: string;
    buyDrinkCount?: number;
}
