export interface Location {
    // Defined by database
    _id: string;
    locationName: string;

    // Local attributes
    imgSource?: string;
}
