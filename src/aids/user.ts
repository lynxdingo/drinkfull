export interface User {
    _id: string;
    firstname?: string;
    lastname?: string;
    email: string;
    password: string;
    locationid?: string;
    roomid?: string;
    currentLocationId?: string;
    dateAdded?: number;
    countDonations?: number;
    monthlyLimit?: number;
    bankingData?: string;
    isAdmin?: string; // should be boolean
    movedout?: string; // should be boolean
}
