export interface Order {
    _id?: string;
    productId: string;
    locationId: string;
    userId: string;
    orderDate: number | string;
    price: string;
    deleted: boolean | string;
}
