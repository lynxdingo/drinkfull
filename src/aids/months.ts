export const MONTHS: any[] = [
    {
        id: '0',
        name: 'Januar',
        days: 31,

    },
    {
        id: '1',
        name: 'Februar',
        days: 28,
    },
    {
        id: '2',
        name: 'März',
        days: 31,
    },
    {
        id: '3',
        name: 'April',
        days: 30,
    },
    {
        id: '4',
        name: 'Mai',
        days: 31,
    },
    {
        id: '5',
        name: 'Juni',
        days: 30,
    },
    {
        id: '6',
        name: 'Juli',
        days: 31,
    },
    {
        id: '7',
        name: 'August',
        days: 31,
    },
    {
        id: '8',
        name: 'September',
        days: 30,
    },
    {
        id: '9',
        name: 'Oktober',
        days: 31,
        
    },
    {
        id: '10',
        name: 'November',
        days: 30,
    },
    {
        id: '11',
        name: 'Dezember',
        days: 31,
    }
];
